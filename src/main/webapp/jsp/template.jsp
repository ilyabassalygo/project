<%@include file="./libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="cols col-6">
            <jsp:include page="locale.jsp" flush="true"/>
            <a class="btn" href="controller?command=navigation&path=/jsp/hello.jsp"> <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>