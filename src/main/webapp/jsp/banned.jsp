<%@include file="./libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="simple-form cols col-6">
            You been banned till
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>
