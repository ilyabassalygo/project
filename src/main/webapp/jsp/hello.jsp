<%@include file="./libs.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="label.title"/></title>
    <link rel="stylesheet" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">

    <%@include file="header.jsp" %>
    <div class="row">
        <div class="cols col-2">
        </div>
        <div class="cols col-9">
            <div class="simple-form">
                <h1><fmt:message key="label.welcome"/></h1>
                <div class="simple-form">
                    <img src="${pageContext.request.contextPath}/files/images/casino.jpg">
                </div>
                <div id="news">
                    <c:forEach var="news" items="${applicationScope.news}" varStatus="status">
                        <section class="content-news">
                            <h2>${news.head}</h2>
                            <img src="show?type=news&id=${news.newsId}" alt="news ${news.newsId}" width="500"
                                 height="250px"/>
                            <p>${news.text}</p>
                        </section>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</body>
</html>