<%@include file="libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="header.jsp" %>
    <div class="head"></div>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="cols col-6">
            <form class="simple-form" action="controller" method="POST">
                ${message}
                <input type="hidden" name="command" value="login"/>
                <fmt:message key="label.login"/>:<br>
                <input class="simple-text" type="text" name="login" required/><br>
                <fmt:message key="label.password"/>:<br>
                <input class="simple-text" type="password" name="password" required/><br>
                <input class="btn" type="submit" name="submit" value="<fmt:message key="label.submit" />">
                <a class="btn"
                   href="controller?command=navigation&path=<fmt:message key="path.register" bundle="${path}"/>"><fmt:message
                        key="label.register"/></a>
            </form>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>