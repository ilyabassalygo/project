<%@include file="libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
</head>
<body>
<div class="row">
    <div class="cols col-12">
        <ul class="menu-left">
            <a href="${pageContext.request.contextPath}/controller?command=change_locale&locale=en_US&path=${pageContext.request.servletPath}">
                <li>eu</li>
            </a>
            <a href="${pageContext.request.contextPath}/controller?command=change_locale&locale=ru_RU&path=${pageContext.request.servletPath}">
                <li>ru</li>
            </a>
        </ul>
        <ul class="menu">
            <c:if test="${sessionScope.user.role eq 'guest'}">
                <a href="controller?command=navigation&path=<fmt:message key="path.login" bundle="${path}"/>">
                    <li><fmt:message key="label.enter"/></li>
                </a>
            </c:if>
            <c:if test="${sessionScope.user.role eq 'guest' or sessionScope.user.role eq 'banned' or sessionScope.user.role eq 'user'}">
                <a href="controller?command=show_news">
                    <li><fmt:message key="label.news"/></li>
                </a>
                <a href="#">
                    <li><fmt:message key="label.rules"/></li>
                </a>
                <a href="#">
                    <li><fmt:message key="label.contacts"/></li>
                </a>
            </c:if>
            <c:if test="${sessionScope.user.role eq 'user'}">
                <a href="controller?command=game">
                    <li><fmt:message key="label.play"/></li>
                </a>
            </c:if>
            <c:if test="${sessionScope.user.role eq 'user' or sessionScope.user.role eq 'banned'}">
                <a href="controller?command=profile">
                    <li><fmt:message key="label.profile"/></li>
                </a>
                <a href="controller?command=logout">
                    <li><fmt:message key="label.logout"/></li>
                </a>
            </c:if>
            <c:if test="${sessionScope.user.role eq 'admin'}">
                <a href="controller?command=news">
                    <li><fmt:message key="label.news"/></li>
                </a>
                <a href="controller?command=users_info">
                    <li><fmt:message key="label.users"/></li>
                </a>
                <a href="controller?command=messages">
                    <li><fmt:message key="label.messages"/></li>
                </a>
                <a href="controller?command=logout">
                    <li><fmt:message key="label.logout"/></li>
                </a>
            </c:if>
        </ul>
    </div>
</div>
</body>
</html>
