<%@include file="./libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="header.jsp" %>
    <div class="head"></div>
    <div class="row">
        <div class="cols col-4"></div>
        <div class="cols col-4">
            <form class="simple-form" action="controller" method="POST">
                <p>${error_register}<p>
                <input type="hidden" name="command" value="register"/>
                <fmt:message key="label.login"/>:<br>
                <input class="simple-text" type="text" name="login" pattern="[\w]{6,}" required/>*<br>
                <span class="err" id="err-psw1"></span>
                <fmt:message key="label.password"/>:<br>
                <input class="simple-text" type="password" name="password"
                       pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{8,}$" required/>*<br>
                <span class="err" id="err-psw2"></span>
                <fmt:message key="message.repeat-password"/> :<br>
                <input class="simple-text" type="password" name="password-check"
                       pattern="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{8,}$" required/>*<br>
                <fmt:message key="label.username"/>:<br>
                <input class="simple-text" type="text" name="name"/><br>
                <span class="err" id="err-bday"></span>
                <fmt:message key="message.enter_bday"/>:<br>
                <input id="bday" class="simple-text" type="date" name="bday" min="1900-01-01" required><br>
                <span class="err" id="err-email"></span>
                <fmt:message key="label.email"/>:<br>
                <input class="simple-text" type="text" name="email"
                       pattern="(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)" required/>*<br>
                <input class="btn" type="submit" name="submit" value=" <fmt:message key="label.submit" />">
            </form>
            <br>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.login" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-4"></div>
    </div>
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-3.2.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/roulette.js"></script>
<script src="${pageContext.request.contextPath}/js/validate.js"></script>
</body>
</html>
