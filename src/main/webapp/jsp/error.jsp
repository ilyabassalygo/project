<%@include file="./libs.jsp" %>
<html>
<head>
    <title>Error Page</title>
    <meta charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <div class="head"></div>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="simple-form cols col-6">
            Request from ${pageContext.errorData.requestURI} is failed
            <br/>
            Servlet name or type: ${pageContext.errorData.servletName}
            <br/>
            Status code: ${pageContext.errorData.statusCode}
            <br/>
            Exception: ${pageContext.errorData.throwable}
            <br>
            <a class="btn"
               href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>"><fmt:message
                    key="label.back"/> </a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>