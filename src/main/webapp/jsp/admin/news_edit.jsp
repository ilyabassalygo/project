<%@include file="../libs.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="label.title"/></title>
    <link rel="stylesheet" href="css/styles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">

    <%@include file="../header.jsp" %>

    <div class="row">
        <div class="cols col-5">
            <form class="simple-form" action="controller" method="post">
                <h1><fmt:message key="label.add_news"/></h1>
                <input type="hidden" name="command" value="add_news">
                <fmt:message key="message.theme"/><br>
                <input class="simple-text" type="text" name="head"><br>
                <fmt:message key="message.text_message"/>
                <textarea class="simple-text" name="text" cols="40" rows="5"></textarea><br>
                <input class="btn" type="submit" value="<fmt:message key="label.add"/>">
            </form>
        </div>

        <div class="cols col-7">
            <c:forEach var="news" items="${applicationScope.news}" varStatus="status">
                <div class="simple-form">
                    <button class="btn" onclick="showHidden(${status.index})"><fmt:message
                            key="label.edit"/></button>
                    <div id="edit${status.index}" style="display: none;">
                        <form class="simple-form" action="controller" method="post">
                            <input type="hidden" name="command" value="edit_news">
                            <input type="hidden" name="index" value="${status.index}">
                            <h2><fmt:message key="label.edit"/></h2>
                            <fmt:message key="message.theme"/><br>
                            <input class="simple-text" type="text" name="head"><br>
                            <fmt:message key="message.text_message"/><br>
                            <textarea class="simple-text" name="text" cols="40" rows="5"></textarea><br>
                            <input class="btn" type="submit" value="<fmt:message key="label.submit"/>">
                        </form>

                        <form class="simple-form" action="upload" method="post" enctype="multipart/form-data">
                            <h2><fmt:message key="label.load_news"/></h2>
                            <input type="hidden" name="news_id" value="${news.newsId}">
                            <input class="simple-text" type="file" name="file"/>
                            <input type="hidden" value="news" name="upload_picture"/>
                            <br/>
                            <input class="btn" type="submit" value="<fmt:message key="label.submit"/> "/><br>
                        </form>
                    </div>
                    <script>
                        function showHidden(index) {
                            var id = "edit" + index;
                            console.log(index + " " + id);
                            if (document.getElementById(id).style.display == "none") {
                                document.getElementById(id).style.display = "block"
                            }
                            else {
                                document.getElementById(id).style.display = "none"
                            }
                        }
                    </script>

                    <h2>ID ${news.newsId}</h2>
                    <h2>${news.head}</h2>
                    <img src="show?type=news&id=${news.newsId}" alt="news ${news.newsId}" height="300px"
                         width="300px"/>
                    <p>${news.text}</p>
                    <a class="btn" href="controller?command=delete_news&index=${status.index}"><fmt:message
                            key="label.delete"/></a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
</div>
</body>
</html>