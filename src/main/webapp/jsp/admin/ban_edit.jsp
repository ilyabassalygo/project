<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="cols col-6">
            ${message}
            <form class="simple-form" action="controller" method="post">
                <input type="hidden" name="command" value="ban">
                Id : ${sessionScope.banId}<br>
                <fmt:message key="message.ban.choose"/><br>
                <select class="simple-text" name="type">
                    <c:forEach var="type" items="${applicationScope.banType}" varStatus="status">
                        <option value="${type.type}">${type.type}</option>
                    </c:forEach>
                </select><br>
                ${sessionScope.banDate}<br>
                <fmt:message key="label.ban-table.reason"/><br>
                <input class="simple-text" type="text" name="reason"><br>
                <input class="btn" type="submit" value="<fmt:message key="label.submit"/>">
            </form>
            <a class="btn"
               href="controller?command=navigation&path=<fmt:message key="path.users_info" bundle="${path}"/>">
                <fmt:message
                        key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>