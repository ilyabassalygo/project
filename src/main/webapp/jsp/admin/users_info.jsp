<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-2"></div>
        <div class="simple-form cols col-8">
            <h2><fmt:message key="title.users-list"/></h2>
            <form action="controller" method="post">
                <input type="hidden" name="command" value="ban_user">
                <table class="demo">
                    <tr>
                        <th><fmt:message key="label.login"/></th>
                        <th><fmt:message key="label.name"/></th>
                        <th><fmt:message key="label.userlist-table.ban"/></th>
                    </tr>
                    <c:forEach var="user" items="${applicationScope.users}" varStatus="status">
                        <tr>
                            <td>${user.login}</td>
                            <td>${user.name}</td>
                            <td><input class="btn" type="submit" name="${user.userId}"
                                       value="<fmt:message key="label.submit"/> "/></td>
                        </tr>
                    </c:forEach>
                </table>
            </form>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-2"></div>
    </div>
    <div class="row">
        <div class="cols col-2"></div>
        <div class="simple-form cols col-8">
            <h2><fmt:message key="title.banned-users"/></h2>
            <form action="controller" method="post">
                <input type="hidden" name="command" value="unban_user">
                <table class="demo">
                    <tr>
                        <th><fmt:message key="label.ban-table.type"/></th>
                        <th><fmt:message key="label.login"/></th>
                        <th><fmt:message key="label.name"/></th>
                        <th><fmt:message key="label.ban-table.ban-date"/></th>
                        <th><fmt:message key="label.ban-table.unban-date"/></th>
                        <th><fmt:message key="label.ban-table.reason"/></th>
                        <th><fmt:message key="label.ban-table.unban"/></th>
                    </tr>
                    <c:forEach var="user" items="${applicationScope.bannedUsers}" varStatus="status">
                        <tr>
                            <td>${user.ban.type}</td>
                            <td>${user.user.login}</td>
                            <td>${user.user.name}</td>
                            <td>${user.banDate}</td>
                            <td>${user.unbanDate}</td>
                            <td>${user.reason}</td>
                            <td><input class="btn" type="submit" name="${user.user.userId}"
                                       value="<fmt:message key="label.submit"/>"></td>
                        </tr>
                    </c:forEach>
                </table>
            </form>
        </div>
        <div class="cols col-2"></div>
    </div>
</div>
</body>
</html>
