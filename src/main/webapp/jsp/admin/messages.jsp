<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-2"></div>
        <div class="simple-form cols col-8">
            ${message}
            <h2><fmt:message key="label.messages"/></h2>
            <form action="controller" method="post">
                <input type="hidden" name="command" value="edit_messages">
                <table class="demo">
                    <tr>
                        <th>Id</th>
                        <th><fmt:message key="label.login"/></th>
                        <th><fmt:message key="label.name"/></th>
                        <th><fmt:message key="label.text"/></th>
                        <th><fmt:message key="label.date"/></th>
                        <th><fmt:message key="label.reply"/></th>
                        <th><fmt:message key="label.reply"/></th>
                        <th><fmt:message key="label.delete"/></th>
                    </tr>
                    <c:forEach var="message" items="${sessionScope.messageList}" varStatus="status">
                        <tr>
                            <td><c:out value="${message.user.userId}"/></td>
                            <td><c:out value="${message.user.login}"/></td>
                            <td><c:out value="${message.user.name}"/></td>
                            <td><c:out value="${message.text}"/></td>
                            <td><c:out value="${message.date}"/></td>
                            <td><textarea class="simple-text" name="reply">${message.reply}</textarea></td>
                            <td><input class="btn" type="submit" name="${message.messageId}"
                                       value="<fmt:message key="message.reply"/>"/></td>
                            <td><a class="btn" href="controller?command=delete_messages&mes_id=${message.messageId}">
                                x</a>
                        </tr>
                    </c:forEach>
                </table>
            </form>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-2"></div>
    </div>
</div>
</body>
</html>