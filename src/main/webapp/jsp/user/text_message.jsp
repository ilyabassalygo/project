<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="simple-form cols col-6">
            ${message}
            <form action="controller" method="post">
                <input type="hidden" name="command" value="text_message"/>
                <h2><fmt:message key="message.text_message"/></h2>
                <textarea class="simple-text" name="message_text" cols="40" rows="5"></textarea><br><br>
                <input class="btn" type="submit" value="<fmt:message key="label.submit"/>"/>
            </form>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.game" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>