<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="cols col-6">
            <h2></h2>
            <form class="simple-form" action="upload" method="post" enctype="multipart/form-data">
                <h2><fmt:message key="message.load_avatar"/></h2>
                <input class="simple-text" type="file" name="file"/>
                <input type="hidden" value="avatar" name="upload_picture"/>
                <br/>
                <input class="btn" type="submit" value="<fmt:message key="label.download-avatar"/> "/><br>
            </form>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.profile" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>