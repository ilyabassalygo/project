<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <div class="head"></div>
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-4"></div>
        <div class="cols col-4">
            <p>${error_edit}</p>
            <p>${error_register}<p>
            <form class="simple-form" action="controller" method="POST">
                <input type="hidden" name="command" value="edit_profile"/>
                <fmt:message key="label.username"/><br>
                <input class="simple-text" type="text" name="name"/><br>
                <fmt:message key="label.bday"/> :<br>
                <input class="simple-text" type="date" name="bday" min="1900-01-01"><br>
                <fmt:message key="label.email"/>:<br>
                <input class="simple-text" type="text" name="email"
                       pattern="(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$)"/><br>
                <input class="btn" type="submit" name="submit" value=" <fmt:message key="label.submit" />">
            </form>
            <a class="btn"
               href="controller?command=navigation&path=<fmt:message key="path.load_avatar" bundle="${path}"/>">
                <fmt:message key="label.download-avatar"/></a>
            <br>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.profile" bundle="${path}"/>">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-4"></div>
    </div>
</div>
</body>
</html>