<%@include file="../libs.jsp" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="src/main/webapp/css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="src/main/webapp/css/styles.css"/>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    ${testMessage}
    <div class="row">
        <div class="cols col-1"></div>
        <div class="simple-form cols col-2"><img src="show?type=avatar" alt="${sessionScope.user.imgPath}"
                                                 height="200px" width="200px"></div>
        <div class="simple-form cols col-8">
            ${message}
            <div>
                <table class="userinf">
                    <tr>
                        <td><fmt:message key="label.login"/>:</td>
                        <td>${sessionScope.user.login}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.username"/>:</td>
                        <td>${sessionScope.user.name}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.bday"/>:</td>
                        <td>${sessionScope.user.birthDate}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.email"/>:</td>
                        <td>${sessionScope.user.email}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.cash"/>:</td>
                        <td>${sessionScope.user.cash}</td>
                    </tr>
                </table>
            </div>
            <div>
                <ul class="btnslst">
                    <li>
                        <button class="btn" type="button" onclick="topUpCash()"><fmt:message
                                key="message.increase_cash"/></button>
                    </li>
                    <li>
                        <button class="btn" type="button" onclick="takeCredit()"><fmt:message
                                key="message.add_credit"/></button>
                    </li>
                    <li>
                        <button class="btn" type="button" onclick="activeCredit()"><fmt:message
                                key="message.repay_credit"/></button>
                    </li>
                    <li>
                        <a class="btn"
                           href="controller?command=navigation&path=<fmt:message key="path.editprofile" bundle="${path}"/> ">
                            <fmt:message
                                    key="label.editprofile"/></a>
                    </li>
                    <li>
                        <a class="btn" href="controller?command=statistics"> <fmt:message key="label.statistics"/></a>
                    </li>

                    <li>
                        <a class="btn" href="controller?command=show_user_messages"> <fmt:message
                                key="label.reply"/></a>
                    </li>
                </ul>
            </div>
            <br>
            <div class="cols col-12">
                <div>
                    <jsp:useBean id="check" scope="request" class="java.lang.String"/>
                    <c:if test="${check != false}">
                        <h2><fmt:message key="label.credit_info"/></h2><br>
                        <table class="demo">
                            <tr>
                                <th><fmt:message key="label.types"/></th>
                                <th><fmt:message key="label.credit-lenght"/></th>
                                <th><fmt:message key="label.max-credit"/></th>
                                <th><fmt:message key="label.percents"/></th>
                            </tr>
                            <c:forEach var="info" items="${creditInfo}" varStatus="creditInfo">
                                <tr>
                                    <td>${info.type}</td>
                                    <td>${info.paymentDays}</td>
                                    <td>${info.maxPayment}</td>
                                    <td>${info.percent}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>
                </div>
                <div id="top-up-cash" style="display: none">
                    <form class="simple-form" action="controller" method="post">
                        <input type="hidden" name="command" value="top_up_cash"/>
                        <fmt:message key="message.enter_summ"/><br>
                        <input class="simple-text" type="text" name="cash_increase_value"
                               pattern="([\d]+)\.([\d]{2})||([\d]+)"/>
                        <input class="btn" type="submit" name="submit" value="<fmt:message key="label.submit"/>"/>
                    </form>
                </div>
                <br>
                <div id="take-credit" style="display: none">
                    <p>${error_take_credit}<p>
                    <form action="controller" method="post" onsubmit="function show (){
                        this.style.display = 'block';
                    }">
                        <input type="hidden" name="command" value="credit_info"/>
                        <input class="btn" type="submit" value="<fmt:message key="label.credit_info"/> "/>
                    </form>

                    <form>
                        <input type="hidden" name="command" value="prepare_credit">
                        <select class="simple-text" name="credit_type"
                                onchange="">
                            <c:forEach var="creditType" items="${sessionScope.creditType}" varStatus="status">
                                <option value="${creditType.type}">${creditType.type}</option>
                            </c:forEach>
                        </select><br><br>
                        <input class="btn" type="submit" value="<fmt:message key="label.take-credit"/>">
                    </form>

                </div>
                <br>
                <form id="active-credit" style="display: none" class="simple-form" action="controller" method="post">
                    <input type="hidden" name="command" value="credit_payment">
                    <p>${message_credit_payment}</p>
                    <table class="demo">
                        <th><fmt:message key="label.ban-table.type"/></th>
                        <th><fmt:message key="label.credit-table.taking-date"/></th>
                        <th><fmt:message key="label.credit-table.repayment-date"/></th>
                        <th><fmt:message key="label.credit-table.payment-amount"/></th>
                        <th><fmt:message key="label.credit-table.payment"/></th>
                        <th><fmt:message key="message.enter_summ"/></th>
                        <c:forEach var="credit" items="${sessionScope.credits}" varStatus="status">
                            <tr>
                                <input type="hidden" name="credit_id" value="${credit.creditId}">
                                <td><c:out value="${ credit.credit.type}"/></td>
                                <td><c:out value="${ credit.takingDate }"/></td>
                                <td><c:out value="${ credit.repaymentDate }"/></td>
                                <td><c:out value="${ credit.paymentAmount }"/></td>
                                <td><c:out value="${ credit.payment }"/></td>
                                <td><input type="text" class="simple-text-table" name="credit_payment_amount"
                                           pattern="([\d]+)\.([\d]{2})||([\d]+)"></td>
                            </tr>
                        </c:forEach>
                    </table>
                    <input type="submit" class="btn" value="<fmt:message key="label.submit"/> "/>
                </form>
                <a class="btn"
                   href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                    <fmt:message
                            key="label.back"/></a>
            </div>
        </div>
        <div class="cols col-1"></div>
    </div>
</div>
<script>
    function topUpCash() {
        var id = "top-up-cash";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
    function takeCredit() {
        var id = "take-credit";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
    function activeCredit() {
        var id = "active-credit";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
</script>
</body>
</html>