<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="cols col-6">
            <jsp:useBean id="credit" scope="session" class="by.epam.bassalygo.roulette.entity.UserHasCredit"/>
            <jsp:useBean id="credType" scope="session" class="by.epam.bassalygo.roulette.entity.Credit"/>
            <form class="simple-form" action="controller" method="post">
                <input type="hidden" name="command" value="take_credit"/>
                <h3><fmt:message key="label.ban-table.type"/></h3>
                ${credType.type}
                <h3><fmt:message key="label.credit-table.taking-date"/></h3>
                ${credit.takingDate}<br>
                <h3><fmt:message key="label.credit-table.repayment-date"/></h3>
                ${credit.repaymentDate}<br>
                <h3><fmt:message key="message.enter_summ"/></h3>
                <h3><fmt:message key="label.max-credit-size"/> ${credType.maxPayment}</h3>
                <input class="simple-text" name="credit_summ" type="number" max="${credType.maxPayment}"
                       pattern="([\d]+)\.([\d]{2})||([\d]+)"/><br>
                <input class="btn" type="submit" value="<fmt:message key="label.submit"/> "/><br>
            </form>
            <a class="btn"
               href="controller?command=navigation&path=<fmt:message key="path.profile" bundle="${path}"/> ">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>