<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-2"></div>
        <div class="cols col-8">
            <form class="simple-form" action="controller" method="get">
                <table class="demo">
                    <tr>
                        <th><fmt:message key="label.name"/></th>
                        <th><fmt:message key="label.text"/></th>
                        <th><fmt:message key="label.date"/></th>
                        <th><fmt:message key="label.reply"/></th>
                        <th><fmt:message key="label.delete"/></th>
                    </tr>
                    <c:forEach var="message" items="${sessionScope.userMessageList}" varStatus="status">
                        <tr>
                            <td><c:out value="${message.user.name}"/></td>
                            <td><c:out value="${message.text}"/></td>
                            <td><c:out value="${message.date}"/></td>
                            <td><c:out value="${message.reply}"/></td>
                            <td><a class="btn"
                                   href="controller?command=delete_messages&mes_id=${message.messageId}&deleted=true">x</a>
                        </tr>
                    </c:forEach>
                </table>
            </form>
            <a class="btn"
               href="controller?command=navigation&path=<fmt:message key="path.profile" bundle="${path}"/> ">
                <fmt:message key="label.back"/></a>
        </div>
        <div class="cols col-2"></div>
    </div>
</div>
</body>
</html>
