<%@include file="../libs.jsp" %>
<html>
<head>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="cols col-3"></div>
        <div class="simple-form cols col-6">
            <table class="userinf">
                ${message}
                <tr>
                    <td><fmt:message key="label.statistics-table.spincount"/>:</td>
                    <td>${sessionScope.spinCount}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.statistics-table.maxwin.spin"/>:</td>
                    <td>${sessionScope.maxWin}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.statistics-table.betcount"/>:</td>
                    <td>${sessionScope.betCount}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.statistics-table.winbetcount"/>:</td>
                    <td>${sessionScope.winBetCount}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.statistics-table.maxwin.bet"/>:</td>
                    <td>${sessionScope.maxWinBet}</td>
                </tr>
                <tr>
                    <td><fmt:message key="label.statistics-table.maxbetsize"/>:</td>
                    <td>${sessionScope.maxBetSize}</td>
                </tr>
            </table>
            <a class="btn" href="controller?command=navigation&path=<fmt:message key="path.profile" bundle="${path}"/>">
                <fmt:message
                        key="label.back"/></a>
        </div>
        <div class="cols col-3"></div>
    </div>
</div>
</body>
</html>
