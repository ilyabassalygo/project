<%@include file="../libs.jsp" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css"/>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    <div class="row">
        <div class="simple-form cols col-6">
            <div>
                ${message}<br>
                <h2>Win Cell</h2>
                <h1>${win_cell}</h1>
                <h2>You win ${total_win}</h2>
                <img id="test" src="show?type=avatar" alt="${sessionScope.user.imgPath}" height="50px" width="50px">
                <h3>${sessionScope.user.cash}</h3>
                <table class="game">
                    <tr>
                        <td rowspan="6" id="cell0" class="le">0</td>
                        <td id="cell3" class="red">
                            3
                        </td>
                        <td id="ver3_6" class="ver"></td>
                        <td id="cell6" class="black">6</td>
                        <td id="ver6_9" class="ver"></td>
                        <td id="cell9" class="red">9</td>
                        <td id="ver9_12" class="ver"></td>
                        <td id="cell12" class="red">12</td>
                        <td id="ver12_15" class="ver"></td>
                        <td id="cell15" class="black">15</td>
                        <td id="ver15_18" class="ver"></td>
                        <td id="cell18" class="red">18</td>
                        <td id="ver18_21" class="ver"></td>
                        <td id="cell21" class="red">21</td>
                        <td id="ver21_24" class="ver"></td>
                        <td id="cell24" class="black">24</td>
                        <td id="ver24_27" class="ver"></td>
                        <td id="cell27" class="red">27</td>
                        <td id="ver27_30" class="ver"></td>
                        <td id="cell30" class="red">30</td>
                        <td id="ver30_33" class="ver"></td>
                        <td id="cell33" class="black">33</td>
                        <td id="ver33_36" class="ver"></td>
                        <td id="cell36" class="red">36</td>
                        <td id="side1" class="le">2to 1</td>
                    </tr>
                    <tr>
                        <td id="hor2_3" class="hor"></td>
                        <td id="cen2_3_5_6" class="cen"></td>
                        <td id="hor5_6" class="hor"></td>
                        <td id="cen5_6_9_8" class="cen"></td>
                        <td id="hor8_9" class="hor"></td>
                        <td id="cen8_9_12_11" class="cen"></td>
                        <td id="hor11_12" class="hor"></td>
                        <td id="cen11_12_15_14" class="cen"></td>
                        <td id="hor14_15" class="hor"></td>
                        <td id="cen14_15_18_17" class="cen"></td>
                        <td id="hor17_18" class="hor"></td>
                        <td id="cen17_18_21_20" class="cen"></td>
                        <td id="hor20_21" class="hor"></td>
                        <td id="cen20_21_24_23" class="cen"></td>
                        <td id="hor23_24" class="hor"></td>
                        <td id="cen23_24_27_26" class="cen"></td>
                        <td id="hor26_27" class="hor"></td>
                        <td id="cen26_27_30_29" class="cen"></td>
                        <td id="hor29_30" class="hor"></td>
                        <td id="cen29_30_33_32" class="cen"></td>
                        <td id="hor32_33" class="hor"></td>
                        <td id="cen32_33_36_35" class="cen"></td>
                        <td id="hor35_36" class="hor"></td>
                    </tr>
                    <tr>
                        <td id="cell2" class="black">2</td>
                        <td id="ver2_5" class="ver"></td>
                        <td id="cell5" class="red">5</td>
                        <td id="ver5_8" class="ver"></td>
                        <td id="cell8" class="black">8</td>
                        <td id="ver8_11" class="ver"></td>
                        <td id="cell11" class="black">11</td>
                        <td id="ver11_14" class="ver"></td>
                        <td id="cell14" class="red">14</td>
                        <td id="ver14_17" class="ver"></td>
                        <td id="cell17" class="black">17</td>
                        <td id="ver17_20" class="ver"></td>
                        <td id="cell20" class="black">20</td>
                        <td id="ver20_23" class="ver"></td>
                        <td id="cell23" class="red">23</td>
                        <td id="ver23_26" class="ver"></td>
                        <td id="cell26" class="black">26</td>
                        <td id="ver26_29" class="ver"></td>
                        <td id="cell29" class="black">29</td>
                        <td id="ver29_32" class="ver"></td>
                        <td id="cell32" class="red">32</td>
                        <td id="ver32_35" class="ver"></td>
                        <td id="cell35" class="black">35</td>
                        <td id="side2" class="le">2 to 1</td>
                    </tr>
                    <tr>
                        <td id="hor1_2" class="hor"></td>
                        <td id="cen1_2_5_4" class="cen"></td>
                        <td id="hor4_5" class="hor"></td>
                        <td id="cen4_5_8_7" class="cen"></td>
                        <td id="hor7_8" class="hor"></td>
                        <td id="cen7_8_11_10" class="cen"></td>
                        <td id="hor10_11" class="hor"></td>
                        <td id="cen10_11_14_13" class="cen"></td>
                        <td id="hor13_14" class="hor"></td>
                        <td id="cen13_14_17_16" class="cen"></td>
                        <td id="hor16_17" class="hor"></td>
                        <td id="cen16_17_20_19" class="cen"></td>
                        <td id="hor19_20" class="hor"></td>
                        <td id="cen19_20_23_22" class="cen"></td>
                        <td id="hor22_23" class="hor"></td>
                        <td id="cen22_23_26_25" class="cen"></td>
                        <td id="hor25_26" class="hor"></td>
                        <td id="cen25_26_29_28" class="cen"></td>
                        <td id="hor28_29" class="hor"></td>
                        <td id="cen28_29_32_31" class="cen"></td>
                        <td id="hor31_32" class="hor"></td>
                        <td id="cen31_32_35_34" class="cen"></td>
                        <td id="hor34_35" class="hor"></td>
                    </tr>
                    <tr>
                        <td id="cell1" class="red">1</td>
                        <td id="ver1_4" class="ver"></td>
                        <td id="cell4" class="black">4</td>
                        <td id="ver4_7" class="ver"></td>
                        <td id="cell7" class="red">7</td>
                        <td id="ver7_10" class="ver"></td>
                        <td id="cell10" class="black">10</td>
                        <td id="ver10_13" class="ver"></td>
                        <td id="cell13" class="black">13</td>
                        <td id="ver13_16" class="ver"></td>
                        <td id="cell16" class="red">16</td>
                        <td id="ver16_19" class="ver"></td>
                        <td id="cell19" class="black">19</td>
                        <td id="ver19_22" class="ver"></td>
                        <td id="cell22" class="black">22</td>
                        <td id="ver22_25" class="ver"></td>
                        <td id="cell25" class="red">25</td>
                        <td id="ver25_28" class="ver"></td>
                        <td id="cell28" class="red">28</td>
                        <td id="ver28_31" class="ver">
                        </td>
                        <td id="cell31" class="black">31</td>
                        <td id="ver31_34" class="ver"></td>
                        <td id="cell34" class="red">34</td>
                        <td id="side3" class="le">2 to 1</td>
                    </tr>

                    <tr>
                        <td id="hor1_2_3" class="hor"></td>
                        <td id="cen1_2_3_6_5_4" class="cen"></td>
                        <td id="hor4_5_6" class="hor"></td>
                        <td id="cen4_5_6_9_8_7" class="cen"></td>
                        <td id="hor7_8_9" class="hor"></td>
                        <td id="cen7_8_9_12_11_10" class="cen"></td>
                        <td id="hor10_11_12" class="hor"></td>
                        <td id="cen10_11_12_15_14_13" class="cen"></td>
                        <td id="hor13_14_15" class="hor"></td>
                        <td id="cen13_14_15_18_17_16" class="cen"></td>
                        <td id="hor16_17_18" class="hor"></td>
                        <td id="cen16_17_18_21_20_19" class="cen"></td>
                        <td id="hor19_20_21" class="hor"></td>
                        <td id="cen19_20_21_24_23_22" class="cen"></td>
                        <td id="hor22_23_24" class="hor"></td>
                        <td id="cen22_23_24_27_26_25" class="cen"></td>
                        <td id="hor25_26_27" class="hor"></td>
                        <td id="cen25_26_27_30_29_28" class="cen"></td>
                        <td id="hor28_29_30" class="hor"></td>
                        <td id="cen28_29_30_33_32_31" class="cen"></td>
                        <td id="hor31_32_33" class="hor"></td>
                        <td id="cen31_32_33_36_35_34" class="cen"></td>
                        <td id="hor34_35_36" class="hor"></td>
                    </tr>
                    <tr>
                        <td rowspan="2" class="non"></td>
                        <td colspan="8" id="foot1" class="le">1st 12</td>
                        <td colspan="8" id="foot2" class="le">2d 12</td>
                        <td colspan="7" id="foot3" class="le">3d 12s</td>
                    </tr>
                    <tr>
                        <td colspan="4" id="half" class="le">1 to 18</td>
                        <td colspan="4" id="evo1" class="le">even</td>
                        <td colspan="4" id="clr1" class="le">red</td>
                        <td colspan="4" id="clr2" class="le">black</td>
                        <td colspan="4" id="evo2" class="le">odd</td>
                        <td colspan="3" id="hal" class="le">19 to 36</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="simple-form cols col-6">
            <h3><fmt:message key="label.lastbet"/></h3>
            <table id="prev" class="demo">
                <thead>
                <tr>
                    <th><fmt:message key="label.bets-table.type"/></th>
                    <th><fmt:message key="label.bets-table.cells"/></th>
                    <th>win summ</th>
                </tr>
                </thead>
                <c:forEach var="previosBet" items="${previosBets}" varStatus="status">
                    <tr>
                        <td><c:out value="${previosBet.betType.name}"/></td>
                        <td><c:out value="${previosBet.cells}"/></td>
                        <td><c:out value="${previosBet.winSize}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div id="bets_content" class="cols col-6">
            <form id="bets" action="controller" method="post" class="simple-form">
                <input type="hidden" name="command" value="spin"/>

                <h2><fmt:message key="label.bets"/></h2>
                <table id="bets_table" class="demo">
                    <thead>
                    <tr>
                        <th><fmt:message key="label.bets-table.type"/></th>
                        <th><fmt:message key="label.bets-table.cells"/></th>
                        <th><fmt:message key="label.bets-table.previosBet-size"/></th>
                        <th><fmt:message key="label.bets-table.del"/></th>
                    </tr>
                    </thead>
                    <c:forEach var="bet" items="${sessionScope.bets}" varStatus="status">
                        <tr>
                            <td><input class="simple-label" type="text" name="bet_type" value="${bet.betType.name}"
                                       readonly/></td>
                            <td><input id="bettbl" onclick="colorCells()" class="simple-label" type="text"
                                       name="bet_cells" value="${bet.cells}" readonly/></td>

                            <td><input class="simple-label" type="text" name="bet_size" value="${bet.betSize}"
                                       readonly/></td>
                            <td><a class="btn"
                                   href="${pageContext.request.contextPath}/controller?command=del_bet&index=${status.index}">x</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <input class="btn" type="submit" value="<fmt:message key="label.submit"/>"/>

            </form>
        </div>
    </div>
    <div class="row">
        <div class="cols col-2"></div>
        <div class="cols col-8">
            <a class="btn"
               href="${pageContext.request.contextPath}/controller?command=navigation&path=<fmt:message key="path.text_message" bundle="${path}"/>">
                <fmt:message
                        key="label.text-message"/></a>
        </div>
        <div class="cols col-2"></div>
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-3.2.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/roulette.js"></script>
<script src="${pageContext.request.contextPath}/js/color.js"></script>

</body>
</html>