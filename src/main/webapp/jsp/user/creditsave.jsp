<%@include file="../libs.jsp" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="src/main/webapp/css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="src/main/webapp/css/styles.css"/>
    <title><fmt:message key="label.title"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <%@include file="../header.jsp" %>
    ${testMessage}
    <div class="row">
        <div class="cols col-2"></div>
        <div class="simple-form cols col-2"><img src="${sessionScope.img_path}" alt="${sessionScope.img_path}"
                                                 height="200px" width="200px"></div>
        <div class="simple-form cols col-7">
            <div>
                <table class="userinf">
                    <tr>
                        <td><fmt:message key="label.login"/>:</td>
                        <td>${sessionScope.login}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.username"/>:</td>
                        <td>${sessionScope.username}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.bday"/>:</td>
                        <td>${sessionScope.bday}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.email"/>:</td>
                        <td>${sessionScope.email}</td>
                    </tr>
                    <tr>
                        <td><fmt:message key="label.cash"/>:</td>
                        <td>${sessionScope.cash}</td>
                    </tr>
                </table>
            </div>
            <div>
                <ul class="btnslst">
                    <li>
                        <button class="btn" type="button" onclick="topUpCash()"><fmt:message
                                key="message.increase_cash"/></button>
                    </li>
                    <li>
                        <button class="btn" type="button" onclick="takeCredit()"><fmt:message
                                key="message.add_credit"/></button>
                    </li>
                    <li>
                        <button class="btn" type="button" onclick="activeCredit()"><fmt:message
                                key="message.repay_credit"/></button>
                    </li>
                    <li>
                        <a class="btn"
                           href="controller?command=navigation&path=<fmt:message key="path.editprofile" bundle="path"/>">
                            <fmt:message
                                    key="label.editprofile"/></a>
                    </li>
                    <li>
                        <a class="btn" href="controller?command=statistics"> <fmt:message key="label.statistics"/></a>
                    </li>
                </ul>
            </div>
            <br>
            <div class="cols col-12">
                <div id="top-up-cash" style="display: none">
                    <form class="simple-form" action="controller" method="post">
                        <input type="hidden" name="command" value="top_up_cash"/>
                        <fmt:message key="message.enter_summ"/><br>
                        <input class="simple-text" type="text" name="cash_increase_value" pattern="([\d]+)\.([\d]{2})"/>
                        <input class="btn" type="submit" name="submit" value="<fmt:message key="label.submit"/>"/>
                    </form>
                </div>
                <br>
                <div id="take-credit" style="display: none">
                    <form class="simple-form" action="controller" method="POST">
                        <input type="hidden" name="command" value="take_credit"/>
                        <p>${error_take_credit}<p>
                        <form action="controller" method="post">
                            <input type="hidden" name="command" value="credit_info">
                            <select id="credit_type" class="simple-text" name="credit_type" onchange="">
                                <c:forEach var="creditType" items="${sessionScope.creditType}" varStatus="status">
                                    <option value="${creditType.type}">${creditType.type}</option>
                                </c:forEach>
                            </select>
                            <jsp:useBean id="creditInfo" scope="request"
                                         class="by.epam.bassalygo.roulette.entity.Credit"/>
                            <c:if test="${creditInfo} != null">
                                <table class="demo">
                                    <tr>
                                        <th><fmt:message key="label.credit-lenght"/></th>
                                        <th><fmt:message key="label.max-credit"/></th>
                                        <th><fmt:message key="label.percents"/></th>
                                    </tr>
                                    <tr>
                                    <tr>${creditInfo.paymentDays}</tr>
                                    <tr>${creditInfo.maxPayment}</tr>
                                    <tr>${creditInfo.percent}</tr>
                                    </tr>
                                </table>
                            </c:if>
                            <input class="btn" type="submit" value="<fmt:message key="label.take-credit"/> ">
                        </form>
                        <fmt:message key="message.enter_summ"/><br>
                        <input class="simple-text" type="number" name="credit_summ"/><br>
                        <input class="btn" type="submit" value="<fmt:message key="label.submit"/>"/>
                    </form>
                </div>
                <br>
                <form id="active-credit" style="display: none" class="simple-form" action="controller" method="post">
                    <input type="hidden" name="command" value="credit_payment">
                    <p>${message_credit_payment}</p>
                    <table class="demo">
                        <th><fmt:message key="label.credit-table.taking-date"/></th>
                        <th><fmt:message key="label.credit-table.repayment-date"/></th>
                        <th><fmt:message key="label.credit-table.payment-amount"/></th>
                        <th><fmt:message key="label.credit-table.payment"/></th>
                        <th><fmt:message key="label.credit-table.canceled"/></th>
                        <th><fmt:message key="message.enter_summ"/></th>
                        <c:forEach var="credit" items="${sessionScope.credits}" varStatus="status">
                            <tr>
                                <input type="hidden" name="credit_id" value="${credit.creditId}">
                                <td><c:out value="${ credit.takingDate }"/></td>
                                <td><c:out value="${ credit.repaymentDate }"/></td>
                                <td><c:out value="${ credit.paymentAmount }"/></td>
                                <td><c:out value="${ credit.payment }"/></td>
                                <td><c:out value="${ credit.canceled }"/></td>
                                <td><input type="text" class="simple-text-table" name="credit_payment_amount"></td>
                            </tr>
                        </c:forEach>
                    </table>
                    <input type="submit" class="btn" value="<fmt:message key="label.submit"/> "/>
                </form>
                <a class="btn"
                   href="controller?command=navigation&path=<fmt:message key="path.hello" bundle="${path}"/>">
                    <fmt:message
                            key="label.back"/></a>
            </div>
        </div>
        <div class="cols col-1"></div>
    </div>
</div>
<script>
    function topUpCash() {
        var id = "top-up-cash";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
    function takeCredit() {
        var id = "take-credit";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
    function activeCredit() {
        var id = "active-credit";
        if (document.getElementById(id).style.display == "none") {
            document.getElementById(id).style.display = "block"
        }
        else {
            document.getElementById(id).style.display = "none"
        }
    }
</script>
</body>
</html>