var bdayString = document.getElementById("bday").value;
function validate() {
    var result = true;
    var now = new Date();
    var nowYear = now.getFullYear();
    var bdayString = document.getElementById("bday").value;
    var date = bdayString.split("-");
    var year = date[0];
    var PWD_NOT_EQUAL = "*введённые пароли не совпадают",
        BAD_BDAY = "*вам должно быть мин 18 лет и меньше 120";
    var errBday = document.getElementById("err-bday"),
        errPsw1 = document.getElementById("err-psw1"),
        errPsw2 = document.getElementById("err-psw2");
    errPsw1.innerHTML = "";
    errPsw2.innerHTML = "";
    errBday.innerHTML = "";
    var psw1 = document.forms[0]["password"].value,
        psw2 = document.forms[0]["password-check"].value;
    var checkYear = nowYear - year;
    if (checkYear < 18 || checkYear > 120) {
        errBday.innerHTML = BAD_BDAY;
        checkYear = false;
    }
    if (psw1 && psw2 && psw1 !== psw2) {
        errPsw1.innerHTML = PWD_NOT_EQUAL;
        errPsw2.innerHTML = PWD_NOT_EQUAL;
        document.forms[0]["password"].value = "";
        document.forms[0]["password-check"].value = "";
        result = false;
    }
    return result;
}