$(document).ready(function () {
    var cells = $('[id*="cell"]');
    var sides = $('[id*="side"]');
    var foots = $('[id*="foot"]');

    var verticals = $('[id*="ver"]');
    var horizontals = $('[id*="hor"]');
    var centers = $('[id*="cen"]');

    var evenOdd = $('[id*="evo"]');
    var colors = $('[id*="clr"]');
    var lowHalf = $("#half");
    var highHalf = $("#hal");

    var showColorNum = "#cce6ff";

    function mouseOnOne(mas) {
        var i = 0;
        for (i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                this.style.background = showColorNum;
            }
        }
        for (i = 0; i < mas.length; i++) {
            mas[i].onmouseout = function () {
                this.style.background = "transparent";
            }
        }
    }

    mouseOnOne(cells);

    function mouseOnLines(mas) {
        for (var i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                var state = this.getAttribute('id').slice(3);
                var numbers = state.split('_');
                for (var j = 0; j < numbers.length; j++) {
                    var cell = document.getElementById('cell'.concat(numbers[j]));
                    cell.style.background = showColorNum;
                }
            };
            mas[i].onmouseout = function () {
                var state = this.getAttribute('id').slice(3);
                var numbers = state.split('_');
                for (var j = 0; j < numbers.length; j++) {
                    var cell = document.getElementById('cell'.concat(numbers[j]));
                    cell.style.background = "transparent";
                }
            };
        }
    }

    mouseOnLines(verticals);
    mouseOnLines(horizontals);
    mouseOnLines(centers);

    function showOneToTwelve(mas) {
        for (var i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                var state = parseInt(this.getAttribute('id').slice(4));
                var size = 0;
                var j = 1;
                if (state == 1) {
                    j = 1;
                    size = 13;
                }
                if (state == 2) {
                    j = 13;
                    size = 25;
                }
                if (state == 3) {
                    j = 25;
                    size = 37;
                }
                for (j; j < size; j++) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = showColorNum;
                }
                this.style.background = showColorNum;
            };
            mas[i].onmouseout = function () {
                var state = parseInt(this.getAttribute('id').slice(4));
                var size = 0;
                var j = 1;
                if (state == 1) {
                    j = 1;
                    size = 13;
                }
                if (state == 2) {
                    j = 12;
                    size = 25;
                }
                if (state == 3) {
                    j = 24;
                    size = 37;
                }
                for (j; j < size; j++) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = "transparent";
                }
                this.style.background = "transparent";
            };
        }
    }

    showOneToTwelve(foots);

    function showSide(mas) {
        for (var i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                var state = parseInt(this.getAttribute('id').slice(4));
                var j = 0;
                if (state == 1) {
                    j = 3;
                }
                if (state == 2) {
                    j = 2;
                }
                if (state == 3) {
                    j = 1;
                }
                for (j; j < cells.length; j = j + 3) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = showColorNum;
                }
                this.style.background = showColorNum;
            };
            mas[i].onmouseout = function () {
                var state = parseInt(this.getAttribute('id').slice(4));
                var size = 0;
                var j = 1;
                if (state == 1) {
                    j = 3;
                }
                if (state == 2) {
                    j = 2;
                }
                if (state == 3) {
                    j = 1;
                }
                for (j; j < cells.length; j = j + 3) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = "transparent";
                }
                this.style.background = "transparent";
            };
        }
    }

    showSide(sides);

    function showEvenOdd(mas) {
        for (var i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                var state = parseInt(this.getAttribute('id').slice(3));
                var j = 0;
                if (state == 1) {
                    j = 2;
                }
                if (state == 2) {
                    j = 1;
                }
                for (j; j < cells.length; j = j + 2) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = showColorNum;
                }
                this.style.background = showColorNum;
            };
            mas[i].onmouseout = function () {
                var state = parseInt(this.getAttribute('id').slice(3));
                var j = 0;
                if (state == 1) {
                    j = 2;
                }
                if (state == 2) {
                    j = 1;
                }
                for (j; j < cells.length; j = j + 2) {
                    var cell = document.getElementById('cell'.concat(j));
                    cell.style.background = "transparent";
                }
                this.style.background = "transparent";
            };
        }
    }

    showEvenOdd(evenOdd);

    function showColor(mas) {
        var redNumbers = [1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36];
        var blackNumbers = [2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35];
        for (var i = 0; i < mas.length; i++) {
            mas[i].onmouseover = function () {
                var state = parseInt(this.getAttribute('id').slice(3));
                var cell;
                if (state == 1) {
                    for (var j = 0; j < redNumbers.length; j++) {
                        cell = document.getElementById('cell'.concat(redNumbers[j]));
                        cell.style.background = showColorNum;
                    }
                }
                if (state == 2) {
                    for (j = 0; j < blackNumbers.length; j++) {
                        cell = document.getElementById('cell'.concat(blackNumbers[j]));
                        cell.style.background = showColorNum;
                    }
                }
                this.style.background = showColorNum;
            };
            mas[i].onmouseout = function () {
                var state = parseInt(this.getAttribute('id').slice(3));
                this.style.background = "transparent";
                var cell;
                if (state == 1) {
                    for (var j = 0; j < redNumbers.length; j++) {
                        cell = document.getElementById('cell'.concat(redNumbers[j]));
                        cell.style.background = "transparent";
                    }
                }
                if (state == 2) {
                    for (j = 0; j < blackNumbers.length; j++) {
                        cell = document.getElementById('cell'.concat(blackNumbers[j]));
                        cell.style.background = "transparent";
                    }
                }
            };
        }
    }

    showColor(colors);

    lowHalf[0].onmouseover = function () {
        for (var i = 0; i < cells.length; i++) {
            var numb = cells[i].getAttribute('id').slice(4);
            if (numb != 0 && numb < 19) {
                cells[i].style.background = showColorNum;
            }
        }
        this.style.background = showColorNum;
    };
    lowHalf[0].onmouseout = function () {
        for (var i = 0; i < cells.length; i++) {
            var numb = cells[i].getAttribute('id').slice(4);
            if (numb != 0 && numb < 19) {
                cells[i].style.background = "transparent";
            }
        }
        this.style.background = "transparent";
    };

    highHalf[0].onmouseover = function () {
        for (var i = 0; i < cells.length; i++) {
            var numb = cells[i].getAttribute('id').slice(4);
            if (numb > 18 && numb < 37) {
                cells[i].style.background = showColorNum;
            }
        }
        this.style.background = showColorNum;
    };
    highHalf[0].onmouseout = function () {
        for (var i = 0; i < cells.length; i++) {
            var numb = cells[i].getAttribute('id').slice(4);
            if (numb > 18 && numb < 37) {
                cells[i].style.background = "transparent";
            }
        }
        this.style.background = "transparent";
    };

    //creates bet form by clicking on table
    function bet(mas) {
        for (var i = 0; i < mas.length; i++) {
            mas[i].onclick = function () {
                var idThis = this.getAttribute('id');
                var betInfo = document.createElement('form');
                betInfo.setAttribute('id', 'bet_info');
                betInfo.setAttribute('class', 'simple-form');
                betInfo.setAttribute('action', 'controller');
                betInfo.setAttribute('method', 'post');
                var divBetContent = document.getElementById('bets_content');

                //type label
                var betType = document.createElement('input');
                betType.setAttribute('class', 'simple-label');
                betType.setAttribute('type', 'text');
                betType.setAttribute('readonly', 'readonly');
                betType.setAttribute('name', 'type');


                //cells list label
                var betCellText = document.createElement('p');
                betCellText.innerHTML = 'bet on cells';

                //cells list
                var betCells = document.createElement('input');
                betCells.setAttribute('class', 'simple-label');
                betCells.setAttribute('type', 'text');
                betCells.setAttribute('name', 'cell');
                betCells.setAttribute('readonly', 'readonly');

                var horP = new RegExp('(hor)|(ver)');
                var cenP = new RegExp('cen');
                var sideP = new RegExp('side');
                var footP = new RegExp('foot');
                var evoP = new RegExp('evo');
                var colP = new RegExp('clr');


                if (horP.test(idThis.substr(0, 3))) {
                    var numbs = idThis.slice(3).split('_');
                    console.log(numbs.length);
                    if (numbs.length == 2) {
                        betType.setAttribute('value', 'Split');
                    }
                    if (numbs.length == 3) {
                        betType.setAttribute('value', 'Street');
                    }
                    var str = idThis.slice(3).replace(/_/g, ', ');
                    betCells.setAttribute('value', str);
                }

                if (cenP.test(idThis.substr(0, 3))) {
                    numbs = idThis.slice(3).split('_');
                    if (numbs.length == 4) {
                        betType.setAttribute('value', 'Corner');
                    }
                    if (numbs.length == 6) {
                        betType.setAttribute('value', 'Line');
                    }
                    str = idThis.slice(3).replace(/_/g, ', ');
                    betCells.setAttribute('value', str);
                }

                function sideNumbsF(i) {
                    var ret = '';
                    for (i; i < 37; i += 3) {
                        if (!(i + 3 >= 37)) {
                            ret += i + ', ';
                        } else {
                            ret += i;
                        }
                    }
                    return ret;
                }

                if (sideP.test(idThis.substr(0, 4))) {
                    var number = idThis.slice(4);
                    var sideNumbs = sideNumbsF(parseInt(idThis.slice(4)));
                    betType.setAttribute('value', "Column");
                    betCells.setAttribute('value', sideNumbs);
                }

                function footF(state) {
                    var size = 0;
                    var j = 0;
                    if (state == 1) {
                        j = 1;
                        size = 13;
                    }
                    if (state == 2) {
                        j = 13;
                        size = 25;
                    }
                    if (state == 3) {
                        j = 25;
                        size = 37;
                    }
                    var ret = '';
                    for (j; j < size; j++) {
                        if (!(j + 1 >= size)) {
                            ret += j + ", ";
                        } else {
                            ret += j;
                        }
                    }
                    return ret;
                }

                if (footP.test(idThis.substr(0, 4))) {
                    var state = parseInt(idThis.slice(4));
                    betType.setAttribute('value', "Dozen");
                    betCells.setAttribute('value', footF(state));
                }

                function evoF(state) {
                    var j = 0;
                    var ret = '';
                    if (state == 1) {
                        j = 2;
                    }
                    if (state == 2) {
                        j = 1;
                    }
                    for (j; j < cells.length; j = j + 2) {
                        if (!(j + 2 >= cells.length)) {
                            ret += j + ", ";
                        } else {
                            ret += j;
                        }
                    }
                    return ret;
                }

                if (evoP.test(idThis.substr(0, 3))) {
                    betType.setAttribute('value', 'Even or Odd');
                    betCells.setAttribute('value', evoF(parseInt(idThis.slice(3))));
                }

                function colF(state) {
                    var redNumbers = [1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36];
                    var blackNumbers = [2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35];
                    var ret = '';
                    if (state == 1) {
                        for (var j = 0; j < redNumbers.length; j++) {
                            if (!(j + 1 >= redNumbers.length)) {
                                ret += redNumbers[j] + ", ";
                            } else {
                                ret += redNumbers[j];
                            }
                        }
                    }
                    if (state == 2) {
                        for (j = 0; j < blackNumbers.length; j++) {
                            if (!(j + 1 >= blackNumbers.length)) {
                                ret += blackNumbers[j] + ", ";
                            } else {
                                ret += blackNumbers[j];
                            }
                        }
                    }
                    return ret;
                }

                if (colP.test(idThis.substr(0, 3))) {
                    betType.setAttribute('value', 'Red or Black');
                    betCells.setAttribute('value', colF(parseInt(idThis.slice(3))));
                }

                if (idThis.substr(0, 4) === 'half') {
                    betType.setAttribute('value', 'Low or High');
                    var ret = '';
                    var lowHlf = 19;
                    for (var i = 1; i < lowHlf; i++) {
                        if (!(i + 1 >= lowHlf)) {
                            ret += i + ", ";
                        } else {
                            ret += i;
                        }
                    }
                    betCells.setAttribute('value', ret);
                } else if (idThis.substr(0, 3) === 'hal') {
                    betType.setAttribute('value', 'Low or High');
                    ret = '';
                    var highHlf = 37;
                    for (i = 19; i < highHlf; i++) {
                        if (!(i + 1 >= highHlf)) {
                            ret += i + ", ";
                        } else {
                            ret += i;
                        }
                    }
                    betCells.setAttribute('value', ret);
                }

                var cellPattern = new RegExp('cell[0-9]+');
                if (cellPattern.test(idThis)) {
                    betCells.setAttribute('value', idThis.slice(4));
                    //bet on one cell
                    betType.setAttribute('value', 'Straight up')
                }

                //payment label
                var paymentText = document.createElement('p');
                paymentText.innerHTML = "Enter bet";

                //payment
                var payment = document.createElement('input');
                payment.setAttribute('class', 'simple-text');
                payment.setAttribute('type', 'text');
                payment.setAttribute('name', 'size');
                payment.setAttribute('value', '100');
                payment.setAttribute('id', 'bet_payment');

                var submit = document.createElement('input');
                submit.setAttribute('type', 'submit');
                submit.setAttribute('class', 'btn');
                submit.setAttribute('value', 'ok');


                var del = document.createElement('input');
                del.setAttribute('type', 'button');
                del.setAttribute('class', 'btn');
                del.setAttribute('value', 'x');
                del.onclick = function () {
                    var bye = del.parentNode;
                    clearCell();
                    bye.remove();
                };

                var command = document.createElement('input');
                command.setAttribute('type', 'hidden');
                command.setAttribute('name', 'command');
                command.setAttribute('value', 'set_bet');

                betInfo.appendChild(command);
                betInfo.appendChild(betType);
                betInfo.appendChild(betCellText);
                betInfo.appendChild(betCells);
                betInfo.appendChild(paymentText);
                betInfo.appendChild(payment);
                betInfo.appendChild(submit);
                betInfo.appendChild(del);
                divBetContent.appendChild(betInfo);
            }
        }
    }

    var bettbl = 0;

    function clearCell() {
        for (var i = 0; i < cells.length; i++) {
            cells[i].style.background = 'transparent';
        }
    }

    function getColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    bet(cells);
    bet(verticals);
    bet(horizontals);
    bet(centers);
    bet(sides);
    bet(foots);
    bet(evenOdd);
    bet(colors);
    bet(highHalf);
    bet(lowHalf);


})
;
