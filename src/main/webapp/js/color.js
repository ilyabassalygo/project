var cells = $('[id*="cell"]');
function colorCells(cellIds) {
    cellIds = $('[id*="bettbl"]');
    for (var i = 0; i < cellIds.length; i++) {

        cellIds[i].onclick = function () {
            clearCell();
            var color = getColor();
            var text = this.getAttribute('value');
            var cellMas = text.split(',');
            for (var j = 0; j < cellMas.length; j++) {
                var cell = document.getElementById('cell' + parseInt(cellMas[j]));
                cell.style.background = color;
            }
        };
    }
}

function clearCell() {
    for (var i = 0; i < cells.length; i++) {
        cells[i].style.background = 'transparent';
    }
}

function getColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}