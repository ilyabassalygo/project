package by.epam.bassalygo.roulette.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provide registration validation
 */
public class RegistrationValidator {
    private final static Logger LOGGER = LogManager.getRootLogger();

    /**
     * Checks user birth date
     *
     * @param date user birth date
     * @return status of check
     */
    private boolean checkDate(String date) {
        if (date.equals("")) {
            return false;
        }
        final int MIN_YEAR = 18;
        String[] current = date.split("-");
        int year = Integer.parseInt(current[0]);
        int nowYear = LocalDate.now().getYear();
        return (nowYear - year) >= MIN_YEAR;
    }

    /**
     * Validates registration
     * @param login user login
     * @param password user password
     * @param checkPassword check password
     * @param date user birth date
     * @param email user email
     * @param errorMessage error message
     * @return status of validation
     */
    public boolean validate(String login, String password, String checkPassword, String date, String email, StringBuilder errorMessage) {
        boolean success = true;

        String loginRegExp = "[\\w]{6,}";
        String passwordRegExp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        String emailRegExp = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$)";
        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile(loginRegExp);
        matcher = pattern.matcher(login);
        if (!matcher.matches()) {
            errorMessage.append("Bad login use numbers and english letters");
            success = false;
        }
        pattern = Pattern.compile(passwordRegExp);
        matcher = pattern.matcher(password);
        if (!matcher.matches() || !matcher.matches()) {
            errorMessage.append("Bad password").append('\n');
            success = false;
        }
        if (!password.equals(checkPassword)) {
            errorMessage.append("passwords dont match").append('\n');
            success = false;
        }
        pattern = Pattern.compile(emailRegExp);
        matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            errorMessage.append("bad email").append('\n');
            success = false;
        }
        if (!checkDate(date)) {
            errorMessage.append("you must be at least 18").append('\n');
            success = false;
        }
        return success;
    }
}
