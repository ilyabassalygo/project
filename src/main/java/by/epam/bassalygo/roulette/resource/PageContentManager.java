package by.epam.bassalygo.roulette.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Provides instruments to work with page content properties
 */
public class PageContentManager {
    private static ResourceBundle pageContentBundle;

    private PageContentManager() {
    }

    /**
     * Chooses bundle depended on locale
     *
     * @param locale choosed locale
     */
    public static void chooseBundle(String locale) {
        String[] langReg = locale.split("_");
        Locale find = new Locale.Builder().setLanguage(langReg[0]).setRegion(langReg[1]).build();
        pageContentBundle = ResourceBundle.getBundle("properties/pagecontent", find);
    }

    /**
     * Gets property value by key
     * @param key key value
     * @return property value
     */
    public static String getProperty(String key) {
        return pageContentBundle.getString(key);
    }
}
