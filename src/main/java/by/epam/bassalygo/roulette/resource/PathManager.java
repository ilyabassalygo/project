package by.epam.bassalygo.roulette.resource;

import java.util.ResourceBundle;

/**
 * Provides instruments to work with path properties
 */
public class PathManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("properties/path");

    private PathManager() {
    }

    /**
     * Gets property value by key
     *
     * @param key key value
     * @return property value
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
