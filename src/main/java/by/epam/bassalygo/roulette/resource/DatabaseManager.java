package by.epam.bassalygo.roulette.resource;

import java.util.ResourceBundle;

/**
 * Provides instruments to work with database properties
 */
public class DatabaseManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("properties/database");

    private DatabaseManager() {
    }

    /**
     * Gets property value by key
     *
     * @param key key value
     * @return property value
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
