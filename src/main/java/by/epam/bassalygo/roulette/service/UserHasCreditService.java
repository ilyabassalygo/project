package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.CreditDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.dao.UserHasCreditDAO;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * Provides service layer for UserHasCredit logic
 */
public class UserHasCreditService {
    private final static Logger LOGGER = LogManager.getRootLogger();

    /**
     * Finds all user credits
     *
     * @param id of user
     * @return list if UserHasCredit objects
     */
    public List<UserHasCredit> findCredits(int id) {
        List<UserHasCredit> credits = null;
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            CreditDAO creditDAO = daoHelper.getCreditDAO();

            credits = userHasCreditDAO.findAllByUserId(id);
            for (int i = 0; i < credits.size(); i++) {
                credits.get(i).setCredit(creditDAO.findById(credits.get(i).getCreditId()));
            }
            return credits;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return credits;
    }

    /**
     * Register credit to user
     * @param user user which took credit
     * @param creditType type of credit
     * @param credit userHasCredit object
     * @param payment size of credit
     * @param message error message about taking operation
     * @return status of taking credit (true or false)
     */
    public boolean takeCredit(User user, Credit creditType, UserHasCredit credit, BigDecimal payment, StringBuilder message) {

        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            UserDAO userDAO = daoHelper.getUserDAO();

            BigDecimal procent = new BigDecimal(creditType.getPercent());

            if (creditType.getMaxPayment().compareTo(payment) == -1) {
                message.append("very big payment");
                return false;
            }

            procent = procent.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
            //add credit to cash
            user.setCash(user.getCash().add(payment));
            daoHelper.beginTransaction();
            if (userDAO.update(user)) {
                daoHelper.commit();
            }
            //payment with percents
            payment = payment.add(payment.multiply(procent));

            //setting payment
            credit.setPaymentAmount(payment);
            credit.setPayment(BigDecimal.ZERO);

            if (userHasCreditDAO.create(credit)) {
                message.append("operation successful\n Repayment amount ").append(payment);
                daoHelper.commit();
                return true;
            } else {
                return false;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Transaction error while take credit " + e.getMessage());
            return false;
        }
    }

    /**
     * Checks if user has cash to make payment
     * @param paymentAmount amount of payment
     * @param user user who make payment
     * @param message error message
     * @return status of check (true of false)
     */
    private boolean checkCash(BigDecimal paymentAmount, User user, StringBuilder message) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            if (user.getCash().subtract(paymentAmount).compareTo(BigDecimal.ZERO) >= 0) {
                user.setCash(user.getCash().subtract(paymentAmount));
                return userDAO.update(user);
            } else {
                message.append("you have not enough cash").append('\n');
                return false;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Checks if user credit payment not bigger then  credit payment
     * @param paymentAmount user credit payment amount
     * @param credit type of credit
     * @param userHasCredit userHasCredit object
     * @param user user who made payment
     * @param message error message
     * @return status of check (true or false)
     */
    private boolean checkMaxPayment(BigDecimal paymentAmount, String credit, UserHasCredit userHasCredit, User user, StringBuilder message) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            UserDAO userDAO = daoHelper.getUserDAO();

            Credit creditType = creditDAO.findById(Integer.parseInt(credit));
            if (userHasCredit.getPaymentAmount().compareTo(paymentAmount) <= 0) {
                BigDecimal remnant = paymentAmount.subtract(userHasCredit.getPaymentAmount());
                user.setCash(user.getCash().add(remnant));
                userHasCredit.setCanceled(true);
                message.append("Credit ").append(creditType.getType()).append(" successfully repayed").append('\n');
                if (userDAO.update(user) && userHasCreditDAO.update(userHasCredit)) {
                    this.deleteCredit();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Releases credit payment logic
     * @param payments massive of credit ids
     * @param amounts massive of payment amounts
     * @param user user who made payments
     * @param message error message
     * @return status of payment (true or false)
     */
    public boolean creditPayment(String[] payments, String[] amounts, User user, StringBuilder message) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            for (int i = 0; i < payments.length; i++) {
                UserHasCredit userHasCredit = userHasCreditDAO.findByIds(user.getUserId(), Integer.parseInt(payments[i]));
                BigDecimal paymentAmount = BigDecimal.ZERO;
                if (!amounts[i].equals("")) {
                    paymentAmount = new BigDecimal(amounts[i]);
                }
                BigDecimal result = userHasCredit.getPayment().add(paymentAmount);
                daoHelper.beginTransaction();
                if (checkCash(paymentAmount, user, message)) {
                    if (this.checkMaxPayment(result, payments[i], userHasCredit, user, message)) {
                        userHasCreditDAO.update(userHasCredit);
                    } else {
                        userHasCredit.setPayment(result);
                        userHasCreditDAO.update(userHasCredit);
                        message.append("Payment accepted ").append(result).append('\n');
                    }
                    daoHelper.commit();
                } else {
                    return false;
                }
            }
            return true;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        } catch (SQLException e) {
            LOGGER.log(Level.INFO, "Transaction error during credit payment");
            return false;
        }
    }

    /**
     * Deletes all repayed credits
     */
    private void deleteCredit() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            userHasCreditDAO.deleteAllCanceled();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }
}
