package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.entity.Message;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.MessageDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides service layer for message logic
 */
public class MessageService {
    private static Logger LOGGER = LogManager.getRootLogger();

    //save message in db

    /**
     * Saves message in databaste
     *
     * @param text of message
     * @param id   of user
     * @return true or false
     */
    public boolean writeMessage(String text, int id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            MessageDAO messageDAO = daoHelper.getMessageDAO();
            Message message = new Message();
            message.setText(text);
            message.setUserId(id);
            message.setDate(Timestamp.valueOf(LocalDateTime.now()));
            return messageDAO.create(message);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Finds all messages
     *
     * @return list of Message objects
     */
    public List<Message> findAllMessages() {
        try (DAOHelper daoHelper = new DAOHelper()) {

            MessageDAO messageDAO = daoHelper.getMessageDAO();
            UserDAO userDAO = new UserDAO();
            List<Message> messages = messageDAO.findAll();
            for (Message message : messages) {
                message.setUser(userDAO.findById(message.getUserId()));
            }
            return messages;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Finds all messages sent by user
     *
     * @param id of user
     * @return list of Message objects
     */
    public List<Message> findMessagesByUser(int id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            MessageDAO messageDAO = daoHelper.getMessageDAO();
            UserDAO userDAO = new UserDAO();
            List<Message> allMessages = messageDAO.findAllByUser(id);
            List<Message> messages = new ArrayList<>();
            if (allMessages != null) {
                for (int i = 0; i < allMessages.size(); i++) {
                    Message message = allMessages.get(i);
                    message.setUser(userDAO.findById(message.getUserId()));

                    if (!message.isDeleted()) {
                        messages.add(message);
                    }
                }
            }
            return messages;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Edits message
     * @param reply answer of admin
     * @param id of message
     * @return status of edit operation (true or false)
     */
    public boolean editMessage(String reply, String id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            MessageDAO messageDAO = daoHelper.getMessageDAO();
            int messageId = Integer.parseInt(id);
            Message message = messageDAO.findById(messageId);
            message.setReply(reply);
            LOGGER.log(Level.INFO, id + " " + reply);
            return messageDAO.update(message);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Deletes message
     * @param id of message
     * @param deleted flag
     * @return status of delete operation (true or false)
     */
    public boolean deleteMessage(String id, String deleted) {
        int messageId = Integer.parseInt(id);

        try (DAOHelper daoHelper = new DAOHelper()) {
            MessageDAO messageDAO = daoHelper.getMessageDAO();
            if (deleted != null) {
                Message message = messageDAO.findById(messageId);
                message.setDeleted(true);
                messageDAO.update(message);
                return true;
            } else {
                return messageDAO.delete(messageId);
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }
}
