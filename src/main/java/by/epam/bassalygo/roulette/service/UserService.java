package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.BetDAO;
import by.epam.bassalygo.roulette.dao.SpinDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.encrypt.MDEncryptor;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

/**
 * Provides service layer for user logic
 */
public class UserService {
    public static final Logger LOGGER = LogManager.getRootLogger();
    private final static String DEFAULT_IMG_PATH = "E:\\opt\\Tomcat8\\webapps\\images\\roulette\\jpg.png";

    private final static String ERROR_REGISTER = "Cant register, check your email";
    private final static String ERROR_EXIST = "There is already user with such login";

    /**
     * Finds user by id
     *
     * @param id id of user
     * @return user object
     */
    public User findUser(int id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            return userDAO.findById(id);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
        return null;
    }

    /**
     * Checks login
     * @param user user who try to login
     * @param login login value
     * @param password password value
     * @return status of login (true or false)
     * @throws RouletteException
     */
    public boolean checkLogin(User user, String login, String password) throws RouletteException {

        try (DAOHelper daoHelper = new DAOHelper()) {

            UserDAO userDAO = daoHelper.getUserDAO();
            String passwordHash = MDEncryptor.encrypt(password);
            for (User checkUser : userDAO.findAll()) {
                //check of login and password
                if (login.equals(checkUser.getLogin()) &&
                        passwordHash.equals(checkUser.getPassword())) {
                    user.setUserId(checkUser.getUserId());
                    user.setLogin(checkUser.getLogin());
                    user.setPassword(checkUser.getPassword());
                    user.setName(checkUser.getName());
                    user.setRole(checkUser.getRole());
                    user.setBirthDate(checkUser.getBirthDate());
                    user.setEmail(checkUser.getEmail());
                    user.setCash(checkUser.getCash());
                    user.setImgPath(checkUser.getImgPath());
                    return true;
                }
            }
            return false;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Register user
     * @param user user to register
     * @param login login value
     * @param password password value
     * @param name name value
     * @param date birthdate value
     * @param email email value
     * @param errorMessage error message
     * @return status of register (true or false)
     */
    public boolean registerUser(User user, String login, String password, String name, String date, String email, StringBuilder errorMessage) {
        //encrypt password
        String passwordHash = MDEncryptor.encrypt(password);

        user.setLogin(login);
        user.setPassword(passwordHash);
        user.setRole("user");
        user.setName(name);
        user.setBirthDate(Date.valueOf(date));
        user.setEmail(email);
        user.setCash(BigDecimal.ZERO);
        user.setImgPath(DEFAULT_IMG_PATH);

        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            if (userDAO.findByLogin(login) != null) {
                errorMessage.append(ERROR_EXIST).append('\n');
                return false;
            } else {
                if (userDAO.create(user)) {
                    return true;
                } else {
                    errorMessage.append(ERROR_REGISTER).append('\n');
                    return false;
                }
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Edits profile of user
     * @param name new name value
     * @param email new email value
     * @param bday new birthdate values
     * @param user user to edit
     * @return status of edit (true or false)
     */
    public boolean editProfile(String name, String email, String bday, User user) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            if (!name.equals("")) {
                user.setName(name);
            }
            if (!email.equals("")) {
                user.setEmail(email);
            }
            if (!bday.equals("")) {
                Date bdate = Date.valueOf(bday);
                user.setBirthDate(bdate);
            }
            return userDAO.update(user);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Finds all active (not banned) users
     *
     * @return list of User objects
     */
    public static List<User> findActiveUsers() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            return userDAO.findAllActive();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Top ups cash
     * @param user user who made payment
     * @param cashIncrease value of payment
     */
    public void topUpCash(User user, BigDecimal cashIncrease) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            daoHelper.beginTransaction();
            BigDecimal cash = user.getCash().add(cashIncrease);
            user.setCash(cash);
            if (userDAO.update(user)) {
                daoHelper.commit();
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "SQL transaction error " + e.getMessage());
        }
    }

    /**
     * Creates statistics of user play
     * @param request
     */
    public void createStatistics(HttpServletRequest request) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            SpinDAO spinDAO = daoHelper.getSpinDAO();
            BetDAO betDAO = daoHelper.getBetDAO();

            HttpSession session = request.getSession();

            User user = (User) session.getAttribute("user");
            int userId = user.getUserId();

            session.setAttribute("spinCount", findUserSpins(spinDAO, userId));
            session.setAttribute("maxWin", findMaxWin(spinDAO, userId));
            session.setAttribute("betCount", betDAO.betsCountByUser(userId));
            session.setAttribute("winBetCount", betDAO.winBetsCountByUser(userId));
            session.setAttribute("maxWinBet", betDAO.findMaxWinByUser(userId));
            session.setAttribute("maxBetSize", betDAO.findMaxBetSizeByUser(userId));
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Finds spins made by user
     *
     * @param dao SpinDAO object
     * @param id  id of user
     * @return count of spins
     * @throws RouletteDAOException
     */
    private BigDecimal findUserSpins(SpinDAO dao, int id) throws RouletteDAOException {
        return dao.spinsCountByUser(id);
    }

    /**
     * Finds max win of user
     *
     * @param dao SpinDAO object
     * @param id  id of user
     * @return max win value
     * @throws RouletteDAOException
     */
    private BigDecimal findMaxWin(SpinDAO dao, int id) throws RouletteDAOException {
        return dao.findMaxWinByUser(id);
    }
}
