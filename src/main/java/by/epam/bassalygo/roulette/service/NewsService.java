package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.NewsDAO;
import by.epam.bassalygo.roulette.entity.News;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Provides service layer for news logic
 */
public class NewsService {
    public static final Logger LOGGER = LogManager.getRootLogger();

    /**
     * Finds all news
     *
     * @return list of News objects
     */
    public List<News> findNews() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            NewsDAO newsDAO = daoHelper.getNewsDAO();
            return newsDAO.findAll();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Creates news
     * @param head of news
     * @param text of news
     * @return status of create operation (true or false)
     */
    public boolean createNews(String head, String text) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            NewsDAO newsDAO = daoHelper.getNewsDAO();
            News news = new News();
            news.setHead(head);
            news.setText(text);
            news.setImgPath("");
            return newsDAO.create(news);

        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Edits news
     * @param newsList list of active news
     * @param index of news to edit
     * @param head new head for current news
     * @param text new text for current news
     * @return status of edit operation (true or false)
     */
    public boolean editNews(List<News> newsList, int index, String head, String text) {

        try (DAOHelper daoHelper = new DAOHelper()) {
            NewsDAO newsDAO = daoHelper.getNewsDAO();
            News news = newsList.get(index);
            if (!head.equals("")) {
                news.setHead(head);
            }
            if (!text.equals("")) {
                news.setText(text);
            }
            return newsDAO.update(news);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Deletes news
     * @param index of deleted news
     * @param newsList list of active news
     * @return status of delete operation (true or false)
     */
    public boolean deleteNews(int index, List<News> newsList) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            NewsDAO newsDAO = daoHelper.getNewsDAO();
            return newsDAO.delete(newsList.get(index).getNewsId());
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }
}
