package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.BetDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.SpinDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.entity.Id;
import by.epam.bassalygo.roulette.entity.Spin;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.parser.BetParser;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

/**
 * Provides service layer for spin logic
 */
public class SpinService {
    private static final Logger LOGGER = LogManager.getRootLogger();

    //check cells for win cell

    /**
     * Check for win cell
     *
     * @param betCellses bet cells
     * @param winCell    win cell number
     * @return status of check (true or false)
     */
    private boolean checkCells(String betCellses, int winCell) {

        final String SPLIT = ", ";
        String[] check = betCellses.split(SPLIT);
        for (String aCheck : check) {
            if (winCell == Integer.parseInt(aCheck)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Calculates win size of bet
     * @param betSize size of bet
     * @param paymentKoef coefficient of bet
     * @return result of calculate
     */
    private BigDecimal sumWinBet(BigDecimal betSize, int paymentKoef) {
        BigDecimal result = betSize;
        result = result.multiply(BigDecimal.valueOf(paymentKoef));
        return result;
    }

    /**
     * Creates spin and calculates result of the spin
     * @param spin current spin
     * @param bets list of active bets
     * @param user user who made spin
     * @param winCell win cell number of spin
     * @param totalWin win amount of spin
     * @return status of creating spin (true or false)
     */
    public boolean spin(Spin spin, List<Bet> bets, User user, int winCell, BigDecimal totalWin) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            if (bets != null) {

                UserDAO userDAO = daoHelper.getUserDAO();
                BigDecimal userCash = user.getCash();

                //for each bet
                for (Bet bet : bets) {

                    //check cells
                    if (checkCells(bet.getCells(), winCell)) {
                        BigDecimal betWin = sumWinBet(bet.getBetSize(),
                                bet.getBetType().getPayment());
                        bet.setWinSize(betWin);
                        //add wib bet size to total win
                        totalWin = totalWin.add(betWin);
                    }
                }

                //set bets spin id
                BetDAO betDAO = daoHelper.getBetDAO();
                for (int i = 0; i < bets.size(); i++) {
                    bets.get(i).setSpinId(spin.getSpinId());
                    betDAO.create(bets.get(i));
                }

                //refresh user cash
                userCash = userCash.add(totalWin);
                user.setCash(userCash);
                daoHelper.beginTransaction();
                if (userDAO.update(user)) {
                    daoHelper.commit();
                }

                //save spin in database
                SpinDAO spinDAO = daoHelper.getSpinDAO();
                spin.setWinAmount(totalWin);
                if (spinDAO.create(spin)) {
                    return true;
                }
            }
            return false;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "ERROR while transaction");
            return false;
        }
    }
}
