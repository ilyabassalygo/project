package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.BanDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.dao.UserHasBanDAO;
import by.epam.bassalygo.roulette.entity.Ban;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasBan;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * Provides service layer for UserHasBan logic
 */
public class UserHasBanService {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private static final String BANNED = "banned";
    private static final String USER = "user";

    private UserHasBan buildUserHasBan(int banId, Timestamp banDate, Timestamp unbanDateTime, String reason, Ban ban) {
        UserHasBan userHasBan = new UserHasBan();
        userHasBan.setUserId(banId);
        userHasBan.setBanId(ban.getBanId());
        userHasBan.setBanDate(banDate);
        userHasBan.setUnbanDate(unbanDateTime);
        userHasBan.setReason(reason);
        return userHasBan;
    }

    /**
     * Finds all banned users and their ban information
     *
     * @return list of UserHasBan objects
     */
    public List<UserHasBan> findBannedUsers() {
        List<UserHasBan> banList = null;
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasBanDAO userHasBanDAO = daoHelper.getUserHasBanDAO();
            banList = userHasBanDAO.findAll();

            BanDAO banDAO = daoHelper.getBanDAO();
            UserDAO userDAO = daoHelper.getUserDAO();

            for (UserHasBan aBanList : banList) {
                Ban ban = banDAO.findById(aBanList.getBanId());
                User user = userDAO.findById(aBanList.getUserId());

                aBanList.setBan(ban);
                aBanList.setUser(user);
            }
            return banList;
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return banList;
    }

    /**
     * Bans user
     * @param banId id of user to ban
     * @param type ban type for user
     * @param date of ban
     * @param reason reason of ban
     * @return status of banning (true or false)
     */
    public boolean banUser(int banId, String type, String date, String reason) {

        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasBanDAO userHasBanDAO = daoHelper.getUserHasBanDAO();
            UserDAO userDAO = daoHelper.getUserDAO();
            BanDAO banDAO = daoHelper.getBanDAO();

            Ban ban = banDAO.findByType(type);
            User user = userDAO.findById(banId);


            Timestamp banDate = Timestamp.valueOf(date.replace("T", " "));

            LocalDateTime unbanDateTime = LocalDateTime.parse(date);
            unbanDateTime = unbanDateTime.plusDays(ban.getDays());

            LocalTime time = ban.getTime().toLocalTime();
            unbanDateTime = unbanDateTime.plusMinutes(time.getMinute());
            unbanDateTime = unbanDateTime.plusHours(time.getHour());

            UserHasBan userHasBan = buildUserHasBan(banId, banDate, Timestamp.valueOf(unbanDateTime), reason, ban);

            user.setRole(BANNED);
            userDAO.update(user);

            return userHasBanDAO.create(userHasBan);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Checks is user banned while login
     * @param user checked user
     */
    public void checkBan(User user) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasBanDAO userHasBanDAO = daoHelper.getUserHasBanDAO();
            UserHasBan userHasBan = userHasBanDAO.findById(user.getUserId());
            if (userHasBan != null) {
                Timestamp now = Timestamp.valueOf(LocalDateTime.now());
                LOGGER.log(Level.INFO, now + " " + userHasBan.getUnbanDate() + " " + userHasBan.getUnbanDate().compareTo(now));
                if (userHasBan.getUnbanDate().compareTo(now) <= 0) {
                    this.unbanUser(user.getUserId());
                }
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Unbans user
     * @param id of unbaning user
     * @return status of unban (true or false)
     */
    public boolean unbanUser(int id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasBanDAO userHasBanDAO = daoHelper.getUserHasBanDAO();
            UserDAO userDAO = daoHelper.getUserDAO();
            User user = userDAO.findById(id);
            if (userHasBanDAO.delete(id)) {
                user.setRole(USER);
                userDAO.update(user);
                return true;
            } else {
                return false;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }
}
