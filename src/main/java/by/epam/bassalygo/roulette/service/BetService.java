package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.BetTypeDAO;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.entity.BetType;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides service layer for bet
 */
public class BetService {
    private static final Logger LOGGER = LogManager.getRootLogger();

    /**
     * Clear cash status  when leaving game.jsp to profile.jsp
     *
     * @param bets list of bets
     * @param user in current session
     */
    public void clearCash(List<Bet> bets, User user) {
        if (bets != null) {
            for (Bet bet : bets) {
                user.setCash(user.getCash().subtract(bet.getBetSize()));
            }
        }
    }

    /**
     * Adds bet to a list of active bets
     * @param bets list of bets
     * @param type of bet
     * @param cell of bet
     * @param size of bet
     * @param user in current session
     * @return status of add operation (true or false)
     */
    public boolean addBet(List<Bet> bets, String type, String cell, String size, User user) {
        Bet bet = new Bet();
        try (DAOHelper daoHelper = new DAOHelper()) {
            BetTypeDAO betTypeDAO = daoHelper.getBetTypeDAO();
            BetType betType = betTypeDAO.findByName(type);
            bet.setBetTypeId(betType.getBetId());
            bet.setBetType(betType);
            bet.setUserId(user.getUserId());
            bet.setCells(cell);
            bet.setBetSize(new BigDecimal(size));
            bet.setWinSize(BigDecimal.ZERO);
            BigDecimal cash = user.getCash();
            if (cash.subtract(bet.getBetSize()).compareTo(BigDecimal.ZERO) < 0) {
                return false;
            } else {
                cash = cash.subtract(bet.getBetSize());
                user.setCash(cash);
                bets.add(bet);
                return true;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return false;
        }
    }

    /**
     * Removes bet from active bets list
     * @param bets list of bets
     * @param user in current session
     * @param index of bet
     * @return status of remove operation (true or false)
     */
    public boolean removeBet(List<Bet> bets, User user, int index) {
        BigDecimal betSize = bets.get(index).getBetSize();
        if (betSize.compareTo(BigDecimal.ZERO) == 1) {
            user.setCash(user.getCash().add(betSize));
            bets.remove(index);
            return true;
        } else {
            return false;
        }
    }
}
