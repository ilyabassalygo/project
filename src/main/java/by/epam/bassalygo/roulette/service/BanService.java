package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.BanDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.entity.Ban;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Provides service layer for ban logic
 */
public class BanService {
    private static final Logger LOGGER = LogManager.getRootLogger();

    /**
     * Finds all bans
     *
     * @return list of Ban objects
     */
    public List<Ban> findAllBans() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            BanDAO banDAO = daoHelper.getBanDAO();
            return banDAO.findAll();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }
}
