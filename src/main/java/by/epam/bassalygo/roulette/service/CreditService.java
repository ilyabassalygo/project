package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.CreditDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Provides service layer for credit logic
 */
public class CreditService {
    private static final Logger LOGGER = LogManager.getRootLogger();

    /**
     * Finds all available credits
     *
     * @return list of Credit objects
     */
    public List<Credit> findAllCreditTypes() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            return creditDAO.findAll();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }

    /**
     * Shows credits available for current user
     * @param id of user
     * @return list of Credit objects
     */
    public List<Credit> findAvailableCreditTypes(int id) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            return creditDAO.findAllAvailable(id);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        return null;
    }
}
