package by.epam.bassalygo.roulette.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Provides encrypt logic
 */
public class MDEncryptor {
    /**
     * Encrypt string by MD algorithm
     *
     * @param string string to encrypt
     * @return result of encrypt
     */
    public static String encrypt(String string) {
        return DigestUtils.md5Hex(string);
    }
}
