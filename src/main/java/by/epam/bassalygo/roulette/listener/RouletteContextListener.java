package by.epam.bassalygo.roulette.listener;

import by.epam.bassalygo.roulette.service.NewsService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener
public class RouletteContextListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent ev) {
        ServletContext context = ev.getServletContext();
        NewsService newsService = new NewsService();
        context.setAttribute("news", newsService.findNews());
    }

    public void contextDestroyed(ServletContextEvent ev) {
    }
}
