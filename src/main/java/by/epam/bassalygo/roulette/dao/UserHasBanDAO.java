package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.UserHasBan;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Extension of AbstractDAO class for UserHasBan entity
 *
 * @see AbstractDAO
 */
public class UserHasBanDAO extends AbstractDAO<UserHasBan> {
    /**
     * SQL query which select all userHasBans
     */
    private static final String SQL_SELECT_ALL_USER_HAS_BAN = "SELECT userId, banId, ban_date, unban_date, reason FROM user_has_ban";
    /**
     * SQL query which insert new userHasBan
     */
    private static final String SQL_INSERT_USER_HAS_BAN = "INSERT INTO user_has_ban(userId, banId, ban_date, unban_date, reason) VALUES (?,?,?,?,?)";
    /**
     * SQL query which finds userHasBan by user id
     */
    private static final String SQL_FIND_USER_HAS_BAN_BY_ID = "SELECT userId, banId, ban_date, unban_date, reason FROM user_has_ban WHERE userId=?";
    /**
     * SQL query which finds userHasBan by user id and ban id
     */
    private static final String SQL_FIND_USER_HAS_BAN_BY_IDS = "SELECT userId, banId, ban_date, unban_date, reason FROM user_has_ban WHERE userId=? and banId=?";
    /**
     * SQL query which updates userHasBan
     */
    private static final String SQL_UPDATE_USER_HAS_BAN = "UPDATE user_has_ban SET userId=?, banId=?, ban_date=?, unban_date=?, reason=? WHERE userId=? and banId=?";
    /**
     * SQL query which deletes userHasBan
     */
    private static final String SQL_DELETE_BAN = "DELETE FROM user_has_ban WHERE userId=?";

    public UserHasBanDAO() {
    }

    public UserHasBanDAO(WrappedConnection connection) {
        super(connection);
    }


    /**
     * Builds UserHasBan object
     * @param resultSet object
     * @return userHasBan object
     * @throws SQLException
     */
    private UserHasBan buildUserHasBan(ResultSet resultSet) throws SQLException {
        UserHasBan ban = new UserHasBan();
        ban.setBanId(resultSet.getInt("banId"));
        ban.setUserId(resultSet.getInt("userId"));
        ban.setBanDate(resultSet.getTimestamp("ban_date"));
        ban.setUnbanDate(resultSet.getTimestamp("unban_date"));
        ban.setReason(resultSet.getString("reason"));
        return ban;
    }

    /**
     * Finds all userHasBans
     * @return list of UserHasBan objects
     * @throws RouletteDAOException
     */
    @Override
    public List<UserHasBan> findAll() throws RouletteDAOException {
        List<UserHasBan> banList = null;
        Statement statement = null;
        try {
            banList = new ArrayList<>();
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_USER_HAS_BAN);
            while (resultSet.next()) {
                banList.add(buildUserHasBan(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasBanDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return banList;
    }

    /**
     * Finds userHasBan by user id
     * @param id of user used to find entity
     * @return userHasBan object
     * @throws RouletteDAOException
     */
    @Override
    public UserHasBan findById(int id) throws RouletteDAOException {
        UserHasBan ban = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_HAS_BAN_BY_ID);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ban = buildUserHasBan(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasBanDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return ban;
    }

    /**
     * Finds userHasBan by banId and userId
     *
     * @param id1 userId
     * @param id2 banId
     * @return userHasBanObjects
     * @throws RouletteDAOException
     */
    public UserHasBan findByIds(int id1, int id2) throws RouletteDAOException {
        UserHasBan ban = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_HAS_BAN_BY_IDS);
            statement.setString(1, String.valueOf(id1));
            statement.setString(2, String.valueOf(id2));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ban = buildUserHasBan(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO findByIds " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return ban;
    }

    /**
     * Deletes userHasBan
     * @param id used to delete entity
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean delete(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_BAN);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasBanDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates userHasBan
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(UserHasBan entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_USER_HAS_BAN);
            if (entity.getUserId() != 0) {
                statement.setInt(1, entity.getUserId());
            }
            if (entity.getBanId() != 0) {
                statement.setInt(2, entity.getBanId());
            }
            statement.setTimestamp(3, entity.getBanDate());
            statement.setTimestamp(4, entity.getUnbanDate());
            statement.setString(5, entity.getReason());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasBanDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * UpdatesUserHasBan
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(UserHasBan entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_USER_HAS_BAN);
            if (entity.getBanDate() != null) {
                statement.setTimestamp(1, entity.getBanDate());
            }
            if (entity.getUnbanDate() != null) {
                statement.setTimestamp(2, entity.getBanDate());
            }
            if (entity.getReason() != null) {
                statement.setString(3, entity.getReason());
            }
            if (entity.getUserId() != 0) {
                statement.setInt(4, entity.getUserId());
            }
            if (entity.getBanId() != 0) {
                statement.setInt(5, entity.getBanId());
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasBanDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
