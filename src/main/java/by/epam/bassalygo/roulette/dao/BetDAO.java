package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for Bet entity
 *
 * @see AbstractDAO
 */
public class BetDAO extends AbstractDAO<Bet> {

    /**
     * SQL query which select all bets
     */
    private static final String SQL_SELECT_ALL_BETS = "SELECT betId, userId, idBet_type, cells, bet_size, win_size, spinId FROM bet";
    /**
     * SQL query which insert bet into table
     */
    private static final String SQL_INSERT_BET = "INSERT INTO bet(userId, idBet_type,cells,bet_size,win_size,spinId) VALUES (?,?,?,?,?,?)";
    /**
     * SQL query which find bet by id
     */
    private static final String SQL_FIND_BET = "SELECT userId, idBet_type, cells, bet_size, win_size, spinId FROM BET WHERE betId=?";
    /**
     * SQL query which delete bets by user id
     */
    private static final String SQL_DELETE_BY_USER = "DELETE FROM bet WHERE userId = ?";
    /**
     * SQL query which counts bets made by user
     */
    private static final String SQL_BET_COUNT_BY_USER = "SELECT COUNT('') AS bet_count FROM bet WHERE userId =?";
    /**
     * SQL query which finds max win amount in bets made by user
     */
    private static final String SQL_FIND_MAX_WIN_AMOUNT_BY_USER = "SELECT MAX(win_size) AS max_win FROM bet WHERE userId = ?";
    /**
     * SQL query which counts win bets made by user
     */
    private static final String SQL_FIND_WIN_BETS_COUNT_BY_USER = "SELECT count(win_size) AS win_bet_count FROM bet WHERE userId = ? and win_size > 0";
    /**
     * SQL query which find mex bet size made by user
     */
    private static final String SQL_FIND_MAX_BET_SIZE_BY_USER = "SELECT MAX(bet_size) AS max_bet_size FROM bet WHERE userId = ?";

    public BetDAO() {
    }

    public BetDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds Bet object
     * @param resultSet object
     * @return bet object
     * @throws SQLException
     */
    private Bet buildBet(ResultSet resultSet) throws SQLException {

        Bet bet = new Bet();
        bet.setBetId(resultSet.getInt("betId"));
        bet.setUserId(resultSet.getInt("userId"));
        bet.setBetTypeId(resultSet.getInt("idBet_type"));
        bet.setCells(resultSet.getString("cells"));
        bet.setBetSize(resultSet.getBigDecimal("bet_size"));
        bet.setWinSize(resultSet.getBigDecimal("win_size"));
        bet.setSpinId(resultSet.getInt("spinId"));
        return bet;
    }

    /**
     * Finds all bets
     * @return list of Bet objects
     * @throws RouletteDAOException
     */
    @Override
    public List<Bet> findAll() throws RouletteDAOException {
        List<Bet> bets = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_BETS);
            while (resultSet.next()) {
                bets.add(this.buildBet(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betDao findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return bets;
    }

    /**
     * Finds bet by id
     * @param id used to find entity
     * @return founded bet object
     * @throws RouletteDAOException
     */
    @Override
    public Bet findById(int id) throws RouletteDAOException {

        Bet bet = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_BET);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                bet = this.buildBet(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betDao findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return bet;
    }

    /**
     * Calculates sql aggregate function
     *
     * @param query  of function
     * @param column to receive
     * @param id     of user
     * @return result of calculate
     * @throws RouletteDAOException
     */
    private BigDecimal aggregate(String query, String column, int id) throws RouletteDAOException {

        BigDecimal result = BigDecimal.ZERO;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(query);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = result.add(new BigDecimal(resultSet.getInt(1)));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betDao aggregate " + query + " " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return result;
    }

    /**
     * Counts all bets made by user
     *
     * @param id of user
     * @return result of counting
     * @throws RouletteDAOException
     */
    public BigDecimal betsCountByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_BET_COUNT_BY_USER, "bet_count", id);
    }

    /**
     * Finds max win amount in bets made by user
     *
     * @param id of user
     * @return max win value
     * @throws RouletteDAOException
     */
    public BigDecimal findMaxWinByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_FIND_MAX_WIN_AMOUNT_BY_USER, "max_win", id);
    }

    /**
     * Counts win bets made by user
     *
     * @param id of user
     * @return result of counting
     * @throws RouletteDAOException
     */
    public BigDecimal winBetsCountByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_FIND_WIN_BETS_COUNT_BY_USER, "win_bet_count", id);
    }

    /**
     * Finds max bet size made by user
     *
     * @param id of user
     * @return max bet size value
     * @throws RouletteDAOException
     */
    public BigDecimal findMaxBetSizeByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_FIND_MAX_BET_SIZE_BY_USER, "max_bet_size", id);
    }

    @Override
    public boolean delete(int id) throws RouletteDAOException {
        return false;
    }

    /**
     * Delete bets made by user
     *
     * @param id of user
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    public boolean deleteByUser(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_BY_USER);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates new bet in database
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(Bet entity) throws RouletteDAOException {

        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_BET);

            statement.setInt(1, entity.getUserId());
            statement.setInt(2, entity.getBetTypeId());
            statement.setString(3, entity.getCells());
            statement.setBigDecimal(4, entity.getBetSize());
            statement.setBigDecimal(5, entity.getWinSize());
            statement.setInt(6, entity.getSpinId());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betDao create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    @Override
    public boolean update(Bet entity) {
        return false;
    }
}
