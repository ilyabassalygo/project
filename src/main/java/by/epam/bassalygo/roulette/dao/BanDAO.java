package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Ban;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for Ban entity
 *
 * @see AbstractDAO
 */
public class BanDAO extends AbstractDAO<Ban> {

    /**
     * SQL query which select all bans
     */
    private static final String SQL_SELECT_ALL_BANS = "SELECT banId, type, days, time FROM ban";
    /**
     * SQL query which insert new ban into table
     */
    private static final String SQL_INSERT_BAN = "INSERT INTO ban(banId, type, days, time) VALUES (?,?,?,?)";
    /**
     * SQL query which finds ban by banId
     */
    private static final String SQL_FIND_BAN = "SELECT banId, type, days, time FROM ban WHERE banId=?";
    /**
     * SQL query which finds ban by type
     */
    private static final String SQL_FIND_BAN_BY_TYPE = "SELECT banId, type, days, time FROM ban WHERE type=?";
    /**
     * SQL query which updates ban
     */
    private static final String SQL_UPDATE_BAN = "UPDATE ban SET banId=?, type=?, days=?, time=? WHERE banId=?";

    public BanDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Method build Ban object
     * @param resultSet object
     * @return ban object
     * @throws SQLException
     */
    private Ban buildBan(ResultSet resultSet) throws SQLException {
        Ban ban = new Ban();
        ban.setBanId(resultSet.getInt("banId"));
        ban.setType(resultSet.getString("type"));
        ban.setTime(resultSet.getTime("time"));
        ban.setDays(resultSet.getInt("days"));
        return ban;
    }


    /**
     * Finds all bans
     * @return list of Ban objects
     * @throws RouletteDAOException
     */
    @Override
    public List<Ban> findAll() throws RouletteDAOException {
        List<Ban> bans = null;
        Statement statement = null;
        try {
            bans = new ArrayList<>();
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_BANS);
            while (resultSet.next()) {
                bans.add(buildBan(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in banDao findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return bans;
    }


    /**
     * Finds ban by id
     * @param id used to find entity
     * @return ban object
     * @throws RouletteDAOException
     */
    @Override
    public Ban findById(int id) throws RouletteDAOException {
        Ban ban = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_BAN);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ban = buildBan(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in banDao findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return ban;
    }

    /**
     * Finds ban by type
     * @param type type of ban
     * @return founded ban object
     * @throws RouletteDAOException
     */
    public Ban findByType(String type) throws RouletteDAOException {

        Ban ban = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_BAN_BY_TYPE);
            statement.setString(1, String.valueOf(type));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ban = buildBan(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in banDao findByType " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return ban;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }

    /**
     * Creates ban in database
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(Ban entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_BAN);
            statement.setInt(1, entity.getBanId());
            statement.setString(2, entity.getType());
            statement.setTime(3, entity.getTime());
            statement.setInt(4, entity.getDays());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in banDao create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates ban
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(Ban entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_BAN);

            if (entity.getType() != null) {
                statement.setString(1, entity.getType());
            }
            if (entity.getTime() != null) {
                statement.setTime(2, entity.getTime());
            }
            if (entity.getDays() != 0) {
                statement.setInt(3, entity.getDays());
            }
            if (entity.getBanId() != 0) {
                statement.setInt(4, entity.getBanId());
            }
            return statement.executeUpdate() == 1;

        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in banDao update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
