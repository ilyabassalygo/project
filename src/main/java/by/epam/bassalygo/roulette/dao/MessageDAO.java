package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Message;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for Message entity
 *
 * @see AbstractDAO
 */
public class MessageDAO extends AbstractDAO<Message> {
    /**
     * SQL query which
     */
    private static final String SQL_SELECT_ALL_MESSAGES = "SELECT messageId, text, date, userId, reply, deleted FROM message";
    /**
     * SQL query which
     */
    private static final String SQL_SELECT_MESSAGES_BY_ID = "SELECT messageId, text, date, userId, reply, deleted FROM message WHERE userId=?";
    /**
     * SQL query which
     */
    private static final String SQL_INSERT_MESSAGE = "INSERT INTO message(text, date, userId, reply, deleted) VALUES (?,?,?,?,?)";
    /**
     * SQL query which
     */
    private static final String SQL_FIND_MESSAGE = "SELECT messageId, text, date, userId, reply, deleted FROM message WHERE messageId=?";
    /**
     * SQL query which
     */
    private static final String SQL_UPDATE_MESSAGE = "UPDATE message SET text=?, date=?, userId=?,reply=?, deleted=? WHERE messageId=?";
    /**
     * SQL query which
     */
    private static final String SQL_DELETE_MESSAGE = "DELETE FROM message WHERE messageId=?";

    public MessageDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds Message object
     * @param resultSet object
     * @return message object
     * @throws SQLException
     */
    private Message buildMessage(ResultSet resultSet) throws SQLException {
        Message message = new Message();
        message.setMessageId(resultSet.getInt("messageId"));
        message.setText(resultSet.getString("text"));
        message.setDate(resultSet.getTimestamp("date"));
        message.setUserId(resultSet.getInt("userId"));
        message.setReply(resultSet.getString("reply"));
        message.setDeleted(resultSet.getBoolean("deleted"));
        return message;
    }

    /**
     * Finds all messages
     * @return list of Message object
     * @throws RouletteDAOException
     */
    @Override
    public List<Message> findAll() throws RouletteDAOException {
        List<Message> messages = null;
        Statement statement = null;
        try {
            messages = new ArrayList<>();
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_MESSAGES);
            while (resultSet.next()) {
                messages.add(buildMessage(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return messages;
    }

    /**
     * Finds all messages wrote by user
     * @param id of user
     * @return list of Message objects
     * @throws RouletteDAOException
     */
    public List<Message> findAllByUser(int id) throws RouletteDAOException {
        List<Message> messages = null;
        PreparedStatement statement = null;
        try {
            messages = new ArrayList<>();
            statement = connection.getPreparedStatement(SQL_SELECT_MESSAGES_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                messages.add(buildMessage(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO findAllByUser " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return messages;
    }

    /**
     * Finds message by id
     * @param id used to find entity
     * @return message object
     * @throws RouletteDAOException
     */
    @Override
    public Message findById(int id) throws RouletteDAOException {
        Message message = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_MESSAGE);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                message = buildMessage(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return message;
    }

    /**
     * Deletes message
     * @param id used to delete entity
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean delete(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_MESSAGE);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates message in database
     * @param entity which needed to create
     * @return status of create operation
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(Message entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_MESSAGE);
            statement.setString(1, entity.getText());
            statement.setTimestamp(2, entity.getDate());
            statement.setInt(3, entity.getUserId());
            statement.setString(4, entity.getReply());
            statement.setBoolean(5, entity.isDeleted());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates message
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(Message entity) throws RouletteDAOException {

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_MESSAGE);
            if (entity.getText() != null) {
                statement.setString(1, entity.getText());
            }
            if (entity.getDate() != null) {
                statement.setTimestamp(2, entity.getDate());
            }
            if (entity.getUserId() != 0) {
                statement.setInt(3, entity.getUserId());
            }
            statement.setString(4, entity.getReply());

            statement.setBoolean(5, entity.isDeleted());
            if (entity.getMessageId() != 0) {
                statement.setInt(6, entity.getMessageId());
            }

            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
