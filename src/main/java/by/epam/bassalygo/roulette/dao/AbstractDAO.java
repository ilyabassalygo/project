package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * The class provides DAO abstraction
 *
 * @param <T> Entity used by DAO
 */
public abstract class AbstractDAO<T> implements AutoCloseable {
    public static final Logger LOGGER = LogManager.getRootLogger();
    /**
     * Wrapped connection to database
     */
    protected WrappedConnection connection;

    /**
     * Default constructor initialize connection by taking it from pool
     */
    public AbstractDAO() {
        try {
            connection = RouletteConnectionPool.getInstance().takeConnection();
        } catch (RouletteException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Constructor which receive connection to initialize
     *
     * @param connection initialize connection
     */
    public AbstractDAO(WrappedConnection connection) {
        this.connection = connection;
    }

    /**
     *  Find all entities in database
     * @return list of founded entity objects
     * @throws RouletteDAOException
     */
    public abstract List<T> findAll() throws RouletteDAOException;

    /**
     * Find entity by id
     * @param id used to find entity
     * @return founded entity object
     * @throws RouletteDAOException
     */
    public abstract T findById(int id) throws RouletteDAOException;

    /**
     * Delete entity by id
     * @param id used to delete entity
     * @return status of operation true or false
     * @throws RouletteDAOException
     */
    public abstract boolean delete(int id) throws RouletteDAOException;

    /**
     * Creates new entity in database
     * @param entity which needed to create
     * @return status of operation true or false
     * @throws RouletteDAOException
     */
    public abstract boolean create(T entity) throws RouletteDAOException;

    /**
     * Update chosen entity
     * @param entity which needed to update
     * @return status of operation true or false
     * @throws RouletteDAOException
     */
    public abstract boolean update(T entity) throws RouletteDAOException;

    /**
     * Closes DAO and returns connection to pool
     * @throws RouletteDAOException
     */
    @Override
    public void close() throws RouletteDAOException {
        connection.returnConnection();
    }

}
