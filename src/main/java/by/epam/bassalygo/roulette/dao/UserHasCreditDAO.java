package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for UserHasCredit entity
 *
 * @see AbstractDAO
 */
public class UserHasCreditDAO extends AbstractDAO<UserHasCredit> {
    /**
     * SQL query which select all userHasCredits from table
     */
    private static final String SQL_SELECT_ALL_USER_HAS_CREDIT = "SELECT userId, creditId, taking_date, repayment_date, payment_amount, payment, canceled FROM user_has_credit";
    /**
     * SQL query which insert new userHasCredit
     */
    private static final String SQL_INSERT_USER_HAS_CREDIT = "INSERT INTO user_has_credit(userId,creditId,taking_date,repayment_date,payment_amount,canceled) VALUES (?,?,?,?,?,?)";
    /**
     * SQL query which finds userHasCredits by user id
     */
    private static final String SQL_FIND_USER_HAS_CREDIT = "SELECT userId, creditId, taking_date, repayment_date, payment_amount, payment, canceled FROM user_has_credit WHERE userId=?";
    /**
     * SQL query which finds userHasCredit by user id and credit id
     */
    private static final String SQL_FIND_USER_HAS_CREDIT_BY_IDS = "SELECT userId, creditId, taking_date, repayment_date, payment_amount, payment, canceled FROM user_has_credit WHERE userId=? and creditId=?";
    /**
     * SQL query which updates userHasCredit
     */
    private static final String SQL_UPDATE_USER_HAS_CREDIT = "UPDATE user_has_credit SET taking_date=?, repayment_date=?, payment_amount=?, payment=?, canceled=? WHERE userId=? and creditId=?";
    /**
     * SQL query which deletes userHasCredits which are canceled
     */
    private static final String SQL_DELETE_USER_HAS_CREDIT_ALL = "DELETE FROM user_has_credit WHERE canceled=true";
    /**
     * SQL query which deletes userHasCredits by user id
     */
    private static final String SQL_DELETE_BY_USER = "DELETE FROM user_has_credit WHERE userId = ?";

    public UserHasCreditDAO() {
    }

    public UserHasCreditDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds UserHasCredit object
     * @param resultSet object
     * @return userHasCredit object
     * @throws SQLException
     */
    private UserHasCredit buildUserHasCredit(ResultSet resultSet) throws SQLException {
        UserHasCredit credit = new UserHasCredit();
        credit.setUserId(resultSet.getInt("userId"));
        credit.setCreditId(resultSet.getInt("creditId"));
        credit.setTakingDate(resultSet.getDate("taking_date"));
        credit.setRepaymentDate(resultSet.getDate("repayment_date"));
        credit.setPaymentAmount(resultSet.getBigDecimal("payment_amount"));
        credit.setPayment(resultSet.getBigDecimal("payment"));
        credit.setCanceled(resultSet.getBoolean("canceled"));
        return credit;
    }

    /**
     * Finds all userHasCredits
     * @return list of UserHasCredit objects
     * @throws RouletteDAOException
     */
    @Override
    public List<UserHasCredit> findAll() throws RouletteDAOException {
        List<UserHasCredit> creditList = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_USER_HAS_CREDIT);
            while (resultSet.next()) {
                creditList.add(buildUserHasCredit(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return creditList;
    }

    /**
     * Finds userHasCredits by user id
     * @param id of user used to find entity
     * @return userHasCredit object
     * @throws RouletteDAOException
     */
    @Override
    public UserHasCredit findById(int id) throws RouletteDAOException {
        UserHasCredit credit = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_HAS_CREDIT);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                credit = buildUserHasCredit(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return credit;
    }

    /**
     * Finds userHasCredit by user id and credit id
     * @param id1 of user
     * @param id2 of credit
     * @return userHasCredit object
     * @throws RouletteDAOException
     */
    public UserHasCredit findByIds(int id1, int id2) throws RouletteDAOException {
        UserHasCredit credit = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_HAS_CREDIT_BY_IDS);
            statement.setString(1, String.valueOf(id1));
            statement.setString(2, String.valueOf(id2));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                credit = buildUserHasCredit(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO findByIds " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return credit;
    }

    /**
     * Finds all userHasCredits by user id
     * @param id of user
     * @return list of UserHasCredit objects
     * @throws RouletteDAOException
     */
    public List<UserHasCredit> findAllByUserId(int id) throws RouletteDAOException {
        List<UserHasCredit> credits = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_HAS_CREDIT);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            credits = new ArrayList<>();
            while (resultSet.next()) {
                credits.add(buildUserHasCredit(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO findAllByUserId " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return credits;
    }

    @Override
    public boolean delete(int id) {

        return false;
    }

    /**
     * Deletes userHasCredit by user id
     *
     * @param id of user
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    public boolean deleteByUser(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_BY_USER);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO delete " + e.getMessage());
        }
    }

    /**
     * Deletes all userHasCredit which are canceled
     * @throws RouletteDAOException
     */
    public void deleteAllCanceled() throws RouletteDAOException {
        try {
            Statement statement = connection.getStatement();
            statement.executeUpdate(SQL_DELETE_USER_HAS_CREDIT_ALL);
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO deleteAllCanceled " + e.getMessage());
        }
    }

    /**
     * Creates userHasCredit
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(UserHasCredit entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_USER_HAS_CREDIT);
            if (entity.getUserId() != 0) {
                statement.setInt(1, entity.getUserId());
            }
            if (entity.getCreditId() != 0) {
                statement.setInt(2, entity.getCreditId());
            }
            statement.setDate(3, entity.getTakingDate());
            statement.setDate(4, entity.getRepaymentDate());
            statement.setBigDecimal(5, entity.getPaymentAmount());
            statement.setBoolean(6, entity.isCanceled());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates userHasCredit
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(UserHasCredit entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_USER_HAS_CREDIT);
            if (entity.getTakingDate() != null) {
                statement.setDate(1, entity.getTakingDate());
            }
            if (entity.getRepaymentDate() != null) {
                statement.setDate(2, entity.getRepaymentDate());
            }
            if (entity.getPaymentAmount() != null) {
                statement.setBigDecimal(3, entity.getPaymentAmount());
            }
            if (entity.getPayment() != null) {
                statement.setBigDecimal(4, entity.getPayment());
            }
            statement.setBoolean(5, entity.isCanceled());
            if (entity.getUserId() != 0) {
                statement.setString(6, String.valueOf(entity.getUserId()));
            }
            if (entity.getCreditId() != 0) {
                statement.setString(7, String.valueOf(entity.getCreditId()));
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserHasCreditDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
