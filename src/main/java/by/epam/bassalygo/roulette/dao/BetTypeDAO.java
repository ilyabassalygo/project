package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.BetType;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for BetType entity
 *
 * @see AbstractDAO
 */
public class BetTypeDAO extends AbstractDAO<BetType> {
    /**
     * SQL query which select all bet types from table
     */
    private static final String SQL_SELECT_ALL_BET_TYPES = "SELECT betId, name, payment FROM bet_type;";
    /**
     * SQL query which finds bet by name
     */
    private static final String SQL_FIND_BET_TYPE_BY_NAME = "SELECT betId, name, payment FROM bet_type WHERE name=?";

    public BetTypeDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds BetType object
     * @param resultSet object
     * @return betType object
     * @throws SQLException
     */
    private BetType buildBetType(ResultSet resultSet) throws SQLException {
        BetType betType = new BetType();
        betType.setBetId(resultSet.getInt("betId"));
        betType.setName(resultSet.getString("name"));
        betType.setPayment(resultSet.getInt("payment"));
        return betType;
    }

    /**
     * Finds all bet types
     * @return list of BetType objects
     * @throws RouletteDAOException
     */
    @Override
    public List<BetType> findAll() throws RouletteDAOException {
        List<BetType> betTypes = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_BET_TYPES);
            while (resultSet.next()) {
                betTypes.add(buildBetType(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betTypeDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return betTypes;
    }

    @Override
    public BetType findById(int id) throws RouletteDAOException {
        return null;
    }

    /**
     * Find betType by name
     * @param name used to find entity
     * @return betType object
     * @throws RouletteDAOException
     */
    public BetType findByName(String name) throws RouletteDAOException {

        BetType betType = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_BET_TYPE_BY_NAME);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                betType = buildBetType(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in betTypeDAO findByName " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return betType;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }

    @Override
    public boolean create(BetType entity) {
        return false;
    }

    @Override
    public boolean update(BetType entity) {
        return false;
    }
}
