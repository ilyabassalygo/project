package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Extension of AbstractDAO class for Credit entity
 *
 * @see AbstractDAO
 */
public class CreditDAO extends AbstractDAO<Credit> {
    private static final String CREDIT_ID = "creditId";
    private static final String TYPE = "type";
    private static final String MAX_PAYMENT_AMOUNT = "max_payment_amount";
    private static final String REPAYMENT_DAYS = "repayment_days";
    private static final String PROCENTS = "procents";

    /**
     * SQL query which select all credits from table
     */
    private static final String SQL_SELECT_ALL_CREDITS = "SELECT credit.creditId, type, max_payment_amount, repayment_days, procents FROM credit";
    /**
     * SQL query which select all available for user credits
     */
    private static final String SQL_SELECT_AVAILABLE_CREDITS = SQL_SELECT_ALL_CREDITS + " left join user_has_credit on credit.creditId = user_has_credit.creditId \n" +
            "where credit.creditId not in (Select user_has_credit.creditId from user_has_credit where userId = ?)";
    /**
     * SQL query which insert new credit into table
     */
    private static final String SQL_INSERT_CREDIT = "INSERT INTO credit(creditId, type, max_payment_amount, repayment_days, procents) VALUES (?,?,?,?,?)";
    /**
     * SQL query which finds credit by id
     */
    private static final String SQL_FIND_CREDIT = "SELECT creditId, type, max_payment_amount, repayment_days, procents FROM credit WHERE creditId=?";
    /**
     * SQL query which finds credit by type
     */
    private static final String SQL_FIND_CREDIT_BY_TYPE = "SELECT creditId, type, max_payment_amount, repayment_days, procents FROM credit WHERE type=?";
    /**
     * SQL query which update credit
     */
    private static final String SQL_UPDATE_CREDIT = "UPDATE credit SET type=?, max_payment_amount=?, repayment_days=?, procents=? WHERE creditId=?";

    public CreditDAO() {
    }

    public CreditDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds Credit object
     * @param resultSet object
     * @return credit object
     * @throws SQLException
     */
    private Credit buildCredit(ResultSet resultSet) throws SQLException {

        Credit credit = new Credit();
        credit.setCreditId(resultSet.getInt(CREDIT_ID));
        credit.setType(resultSet.getString(TYPE));
        credit.setMaxPayment(resultSet.getBigDecimal(MAX_PAYMENT_AMOUNT));
        credit.setPaymentDays(resultSet.getInt(REPAYMENT_DAYS));
        credit.setPercent(resultSet.getInt(PROCENTS));
        return credit;
    }

    /**
     * Finds all credits
     * @return list of Credit objects
     * @throws RouletteDAOException
     */
    @Override
    public List<Credit> findAll() throws RouletteDAOException {
        List<Credit> credits = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_CREDITS);
            while (resultSet.next()) {
                Credit credit = this.buildCredit(resultSet);
                credits.add(credit);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return credits;
    }

    /**
     * Finds all available credits for user
     * @param id of user
     * @return list of Credit objects
     * @throws RouletteDAOException
     */
    public List<Credit> findAllAvailable(int id) throws RouletteDAOException {

        List<Credit> credits = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_SELECT_AVAILABLE_CREDITS);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Credit credit = this.buildCredit(resultSet);
                credits.add(credit);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO findAllAvailable " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return credits;
    }

    /**
     * Finds credit by id
     * @param id used to find entity
     * @return credit object
     * @throws RouletteDAOException
     */
    @Override
    public Credit findById(int id) throws RouletteDAOException {
        Credit credit = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_CREDIT);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                credit = buildCredit(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return credit;
    }

    /**
     * Finds credit by type
     * @param type of credit
     * @return credit object
     * @throws RouletteDAOException
     */
    public Credit findCreditByType(String type) throws RouletteDAOException {
        Credit credit = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_CREDIT_BY_TYPE);
            statement.setString(1, type);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                credit = buildCredit(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO findCreditByType " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return credit;
    }

    @Override
    public boolean delete(int id) {
        return false;
    }

    /**
     * Creates credit in database
     * @param entity which needed to create
     * @return status of create operation (true of false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(Credit entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_CREDIT);
            statement.setInt(1, entity.getCreditId());
            statement.setString(2, entity.getType());
            statement.setBigDecimal(3, entity.getMaxPayment());
            statement.setInt(4, entity.getPaymentDays());
            statement.setInt(5, entity.getPercent());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates credit
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(Credit entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_CREDIT);

            if (entity.getType() != null) {
                statement.setString(1, entity.getType());
            }
            if (entity.getMaxPayment() != null) {
                statement.setBigDecimal(2, entity.getMaxPayment());
            }
            if (entity.getPaymentDays() != 0) {
                statement.setInt(3, entity.getPaymentDays());
            }
            if (entity.getPercent() != 0) {
                statement.setInt(4, entity.getPercent());
            }
            if (entity.getCreditId() != 0) {
                statement.setInt(5, entity.getCreditId());
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in creditDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
