package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Spin;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Extension of AbstractDAO class for Spin entity
 *
 * @see AbstractDAO
 */
public class SpinDAO extends AbstractDAO<Spin> {
    /**
     * SQL query which select all spins from table
     */
    private static final String SQL_SELECT_ALL_SPINS = "SELECT spinId, win_cell_number, win_amount, date, userId FROM spin";
    /**
     * SQL query which insert new spin
     */
    private static final String SQL_INSERT_SPIN = "INSERT INTO spin(spinId,win_cell_number,win_amount,date,userId) VALUES (?,?,?,?,?)";
    /**
     * SQL query which finds spin by id
     */
    private static final String SQL_FIND_SPIN = "SELECT spinId, win_cell_number, win_amount, date, userId FROM spin WHERE spinId=?";
    /**
     * SQL query which finds spins bu user id
     */
    private static final String SQL_FIND_SPINS_BY_USERID = "SELECT spinId,win_cell_number, win_amount, date, userId FROM spin WHERE userId=?";
    /**
     * SQL query which update spin
     */
    private static final String SQL_UPDATE_SPIN = "UPDATE spin SET win_cell_number=?, win_amount=?, date=?, userId=? WHERE spinId=?";
    /**
     * SQL query which counts spins made by user
     */
    private static final String SQL_SPIN_COUNT_BY_USER = "select COUNT('') as spin_count from spin where userId =?";
    /**
     * SQL query which finds max win amount of spin made by user
     */
    private static final String SQL_FIND_MAX_WIN_AMOUNT_BY_USER = "select MAX(win_amount) as max_win from spin where userId = ?";
    /**
     * SQL query which deletes spins by user id
     */
    private static final String SQL_DELETE_BY_USER = "delete from spin where userId = ?";
    /**
     * SQL query which deletes spin by id
     */
    private static final String SQL_DELETE_SPIN = "delete from spin where spinId = ?";

    public SpinDAO() {
    }

    public SpinDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds Spin object
     * @param resultSet object
     * @return spin object
     * @throws SQLException
     */
    private Spin buildSpin(ResultSet resultSet) throws SQLException {
        Spin spin = new Spin();
        spin.setSpinId(resultSet.getInt("spinId"));
        spin.setWinCellNumber(resultSet.getInt("win_cell_number"));
        spin.setWinAmount(resultSet.getBigDecimal("win_amount"));
        spin.setDate(resultSet.getDate("date"));
        spin.setUserId(resultSet.getInt("userId"));
        return spin;
    }

    /**
     * Finds all spins
     * @return list of Spin objects
     * @throws RouletteDAOException
     */
    @Override
    public List<Spin> findAll() throws RouletteDAOException {
        List<Spin> spins = new ArrayList<>();
        Statement statement = null;
        try {
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_SPINS);
            while (resultSet.next()) {
                spins.add(buildSpin(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return spins;
    }

    /**
     * Finds spin by id
     * @param id used to find entity
     * @return spin object
     * @throws RouletteDAOException
     */
    @Override
    public Spin findById(int id) throws RouletteDAOException {
        Spin spin = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_SPIN);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                spin = buildSpin(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return spin;
    }

    /**
     * Finds spins made by user
     * @param id of user
     * @return list of Spin objects
     * @throws RouletteDAOException
     */
    public List<Spin> findAllByUserId(int id) throws RouletteDAOException {
        List<Spin> spins = null;
        PreparedStatement statement = null;

        try {
            spins = new ArrayList<>();
            statement = connection.getPreparedStatement(SQL_FIND_SPINS_BY_USERID);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                spins.add(buildSpin(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO findAllByUserId " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return spins;
    }

    /**
     * Calculates sql aggregate functions
     *
     * @param query of function
     * @param id    of user
     * @return result of calculate
     * @throws RouletteDAOException
     */
    private BigDecimal aggregate(String query, int id) throws RouletteDAOException {

        BigDecimal result = BigDecimal.ZERO;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(query);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = result.add(new BigDecimal(resultSet.getInt(1)));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO aggregate " + query + " " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return result;
    }

    /**
     * Counts spins made by user
     *
     * @param id of user
     * @return result of count
     * @throws RouletteDAOException
     */
    public BigDecimal spinsCountByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_SPIN_COUNT_BY_USER, id);
    }

    /**
     * Finds max win by spin made by user
     *
     * @param id of user
     * @return max win value
     * @throws RouletteDAOException
     */
    public BigDecimal findMaxWinByUser(int id) throws RouletteDAOException {
        return aggregate(SQL_FIND_MAX_WIN_AMOUNT_BY_USER, id);
    }

    /**
     * Deletes spin
     * @param id used to delete entity
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean delete(int id) throws RouletteDAOException {

        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_SPIN);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO delete " + e.getMessage());
        }
    }

    /**
     * Deletes spins by user
     *
     * @param id of user
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    public boolean deleteByUser(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_BY_USER);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates spin
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(Spin entity) throws RouletteDAOException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_INSERT_SPIN);
            statement.setInt(1, entity.getSpinId());
            statement.setInt(2, entity.getWinCellNumber());
            statement.setBigDecimal(3, entity.getWinAmount());
            statement.setDate(4, entity.getDate());
            statement.setInt(5, entity.getUserId());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates spin
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(Spin entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_SPIN);
            if (entity.getWinCellNumber() != -1) {
                statement.setInt(1, entity.getWinCellNumber());
            }
            if (entity.getWinAmount() != null) {
                statement.setBigDecimal(2, entity.getWinAmount());
            }
            if (entity.getDate() != null) {
                statement.setDate(3, entity.getDate());
            }
            if (entity.getUserId() != 0) {
                statement.setInt(4, entity.getUserId());
            }
            if (entity.getSpinId() != 0) {
                statement.setInt(5, entity.getSpinId());
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in SpinDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
