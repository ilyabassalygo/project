package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Extension of AbstractDAO class for User entity
 *
 * @see AbstractDAO
 */
public class UserDAO extends AbstractDAO<User> {
    /**
     * SQL query which select all users from table
     */
    private static final String SQL_SELECT_ALL_USERS = "SELECT userId, login, password, role, name, birth_date, email, cash, img_path FROM user";
    /**
     * SQL query which select all users which not banned
     */
    private static final String SQL_SELECT_ALL_ACTIVE = "SELECT  user.userId, login, password, role, name, birth_date, email, cash, img_path FROM user \n" +
            "where  user.userId not in (Select userId from user_has_ban)";
    /**
     * SQL query which insert new user
     */
    private static final String SQL_INSERT_USER = "INSERT INTO user(login,password,role,name,birth_date,email,img_path) VALUES (?,?,?,?,?,?,?)";
    /**
     * SQL query which finds user by id
     */
    private static final String SQL_FIND_USER = "SELECT userId, login, password, role, name, birth_date, email, cash, img_path FROM user WHERE userId=?";
    /**
     * SQL query which finds user by login
     */
    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT userId, login, password, role, birth_date, name, email, cash, img_path FROM user WHERE login=?";
    /**
     * SQL query which updates user
     */
    private static final String SQL_UPDATE_USER = "UPDATE user SET login=?, password=?, role=?, name=?, birth_date=?, email=?, cash=?, img_path=? WHERE userId=?";
    /**
     * SQL query which deletes user by id
     */
    private static final String SQL_DELETE_USER = "DELETE FROM user WHERE userId = ?";

    public UserDAO() {
    }

    public UserDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds User object
     * @param resultSet object
     * @return user object
     * @throws SQLException
     */
    private User buildUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getInt("userId"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setRole(resultSet.getString("role"));
        user.setName(resultSet.getString("name"));
        user.setBirthDate(resultSet.getDate("birth_date"));
        user.setEmail(resultSet.getString("email"));
        user.setCash(resultSet.getBigDecimal("cash"));
        user.setImgPath(resultSet.getString("img_path"));
        return user;
    }

    /**
     * Select users by query
     * @param query of select
     * @return list of User objects
     * @throws RouletteDAOException
     */
    public List<User> selectUsers(String query) throws RouletteDAOException {
        List<User> users = null;
        Statement statement = null;
        try {
            users = new ArrayList<>();
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                users.add(buildUser(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return users;
    }

    /**
     * Finds all users
     * @return list of User objects
     * @throws RouletteDAOException
     */
    @Override
    public List<User> findAll() throws RouletteDAOException {
        return selectUsers(SQL_SELECT_ALL_USERS);
    }

    /**
     * Finds all users without a ban
     * @return list ou User objects
     * @throws RouletteDAOException
     */
    public List<User> findAllActive() throws RouletteDAOException {
        return selectUsers(SQL_SELECT_ALL_ACTIVE);
    }

    /**
     * Finds user by id
     * @param id used to find entity
     * @return user object
     * @throws RouletteDAOException
     */
    @Override
    public User findById(int id) throws RouletteDAOException {
        User user = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER);
            statement.setString(1, String.valueOf(id));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = buildUser(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return user;
    }

    /**
     * Finds user by login
     * @param login of user
     * @return user object
     * @throws RouletteDAOException
     */
    public User findByLogin(String login) throws RouletteDAOException {
        User user = null;
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_FIND_USER_BY_LOGIN);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = buildUser(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserDAO findByLogin " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }

        return user;
    }

    /**
     * Deletes user
     * @param id used to delete entity
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean delete(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_USER);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in MessageDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates user
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(User entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_USER);
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            statement.setString(3, entity.getRole());
            statement.setString(4, entity.getName());
            statement.setDate(5, entity.getBirthDate());
            statement.setString(6, entity.getEmail());
            statement.setString(7, entity.getImgPath());
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates user
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(User entity) throws RouletteDAOException {

        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_USER);
            if (entity.getLogin() != null) {
                statement.setString(1, entity.getLogin());
            }
            if (entity.getPassword() != null) {
                statement.setString(2, entity.getPassword());
            }
            if (entity.getRole() != null) {
                statement.setString(3, entity.getRole());
            }
            if (entity.getName() != null) {
                statement.setString(4, entity.getName());
            }
            if (entity.getBirthDate() != null) {
                statement.setDate(5, entity.getBirthDate());
            }
            if (entity.getEmail() != null) {
                statement.setString(6, entity.getEmail());
            }
            statement.setBigDecimal(7, entity.getCash());
            if (entity.getImgPath() != null) {
                statement.setString(8, entity.getImgPath());
            }
            if (entity.getUserId() != 0) {
                statement.setString(9, String.valueOf(entity.getUserId()));
            }

            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in UserDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
