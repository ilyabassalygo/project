package by.epam.bassalygo.roulette.dao;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Wrap SQL connection to prevent wild connections
 */
public class WrappedConnection {
    private static final Logger LOGGER = LogManager.getRootLogger();
    /**
     * SQL connection
     */
    private Connection connection;

    /**
     * Constructor which creates wrapped connection
     *
     * @param url      of database
     * @param name     of database user
     * @param password of database user
     */
    WrappedConnection(String url, String name, String password) {
        try {
            connection = DriverManager.getConnection(url, name, password);
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Gets auto commit status
     * @return true or false
     * @throws SQLException
     */
    boolean getAutoCommit() throws SQLException {
        return connection.getAutoCommit();
    }

    /**
     * Set auto commit
     * @param autoCommit true or false
     * @throws SQLException
     */
    public void setAutoCommit(boolean autoCommit) throws SQLException {
        connection.setAutoCommit(autoCommit);
    }

    /**
     * Commit changes
     * @throws SQLException
     */
    public void commit() throws SQLException {
        connection.commit();
    }

    /**
     * Rollback changes
     * @throws SQLException
     */
    public void rollback() throws SQLException {
        connection.rollback();
    }

    /**
     * Checks connection on null
     * @return true or false
     */
    public boolean isNull() {
        return connection == null;
    }

    /**
     * Checks if connection closed
     * @return true or false
     */
    public boolean isClosed() throws SQLException {
        return connection.isClosed();
    }

    /**
     * Gets statement
     * @return statement object
     * @throws SQLException
     */
    public Statement getStatement() throws SQLException {
        return connection.createStatement();
    }

    /**
     * Gets preparedStatement
     * @param statement query
     * @return preparedStatement object
     * @throws SQLException
     */
    public PreparedStatement getPreparedStatement(String statement) throws SQLException {
        return connection.prepareStatement(statement);
    }

    /**
     * Closes statement
     * @param statement which need to close
     */
    public void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, "cant closeCon statement " + e);
            }
        }
    }

    /**
     * Returns connection to pool
     */
    public void returnConnection() {
        RouletteConnectionPool.getInstance().returnConnection(this);
    }

    /**
     * Closes connection
     */
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }
}
