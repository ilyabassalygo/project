package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * Class organize connection to database
 */
public class DAOHelper implements AutoCloseable {
    private static final Logger LOGGER = LogManager.getRootLogger();
    /**
     * Wrapped connection
     */
    private WrappedConnection connection;


    /**
     * Default constructor, initialize connection by taking it from pool
     */
    public DAOHelper() {
        try {
            connection = RouletteConnectionPool.getInstance().takeConnection();
        } catch (RouletteException e) {
            LOGGER.log(Level.ERROR, "Database connection error.");
        }
    }

    /**
     * Creates BanDAO object and initialize it with connection
     *
     * @return BanDAO object
     */
    public BanDAO getBanDAO() {
        return new BanDAO(connection);
    }

    /**
     * Creates BetDAO object and initialize it with connection
     * @return BetDAO object
     */
    public BetDAO getBetDAO() {
        return new BetDAO(connection);
    }

    /**
     * Creates BetTypeDAO object and initialize it with connection
     * @return BetTypeDAO object
     */
    public BetTypeDAO getBetTypeDAO() {
        return new BetTypeDAO(connection);
    }

    /**
     * Creates CreditDAO object and initialize it with connection
     * @return CreditDAO object
     */
    public CreditDAO getCreditDAO() {
        return new CreditDAO(connection);
    }

    /**
     * Creates MessageDAO object and initialize it with connection
     * @return MessageDAO object
     */
    public MessageDAO getMessageDAO() {
        return new MessageDAO(connection);
    }

    /**
     * Creates NewsDAO object and initialize it with connection
     * @return NewsDAO object
     */
    public NewsDAO getNewsDAO() {
        return new NewsDAO(connection);
    }

    /**
     * Creates SpinDAO object and initialize it with connection
     * @return SpinDAO object
     */
    public SpinDAO getSpinDAO() {
        return new SpinDAO(connection);
    }

    /**
     * Creates UserDAO object and initialize it with connection
     * @return UserDAO object
     */
    public UserDAO getUserDAO() {
        return new UserDAO(connection);
    }

    /**
     * Creates UserHasBanDAO object and initialize it with connection
     * @return UserHasBanDAO object
     */
    public UserHasBanDAO getUserHasBanDAO() {
        return new UserHasBanDAO(connection);
    }

    /**
     * Creates UserHasCreditDAO object and initialize it with connection
     * @return UserHasCreditDAO object
     */
    public UserHasCreditDAO getUserHasCreditDAO() {
        return new UserHasCreditDAO(connection);
    }

    /**
     * Begins transaction (set connection auto commit to false)
     * @throws SQLException
     */
    public void beginTransaction() throws SQLException {
        connection.setAutoCommit(false);
    }

    /**
     * Commit changes
     * @throws SQLException
     */
    public void commit() throws SQLException {
        connection.commit();
    }

    /**
     * Closes DAOHelper and returns connection back to pool
     * @throws RouletteDAOException
     */
    @Override
    public void close() throws RouletteDAOException {
        RouletteConnectionPool.getInstance().returnConnection(connection);
    }
}
