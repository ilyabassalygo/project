package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.resource.DatabaseManager;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RouletteConnectionPool {
    private final static Logger LOGGER = LogManager.getRootLogger();
    private static Lock lock = new ReentrantLock();

    private String url = DatabaseManager.getProperty("database.url");
    private String name = DatabaseManager.getProperty("database.username");
    private String password = DatabaseManager.getProperty("database.password");

    private static RouletteConnectionPool instance;
    private BlockingQueue<WrappedConnection> connections;
    private final static int POOL_SIZE = 50;

    private RouletteConnectionPool() {
        connections = new ArrayBlockingQueue<>(POOL_SIZE);
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            for (int i = 0; i < POOL_SIZE; i++) {
                WrappedConnection connection = new WrappedConnection(url, name, password);
                connections.put(connection);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "interrupted " + e.getMessage());
        }
    }

    public static RouletteConnectionPool getInstance() {
        try {
            lock.lock();
            if (instance == null) {
                instance = new RouletteConnectionPool();
            } else {
                return instance;
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    public BlockingQueue<WrappedConnection> getConnections() {
        return connections;
    }

    public WrappedConnection takeConnection() throws RouletteException {
        try {
            if (!connections.isEmpty()) {
                return connections.take();
            }
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
        throw new RouletteException("pool is empty");
    }

    public void returnConnection(WrappedConnection connection) {
        try {
            if (connection == null) {
                LOGGER.log(Level.WARN, "Can't return null connection");
                return;
            }
            if (connection.isNull() || connection.isClosed()) {
                returnBadConnection(connection);
                return;
            }
            try {
                if (!connection.getAutoCommit()) {
                    connection.rollback();
                    connection.setAutoCommit(true);
                }
                this.getConnections().put(connection);
            } catch (SQLException e) {
                LOGGER.log(Level.ERROR, e.getMessage());
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, "Database error");
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, "Process interrupted, while returning connection");
        }
    }

    private boolean fixConnection() throws InterruptedException {
        boolean check;
        WrappedConnection newConnection = new WrappedConnection(url, name, password);
        check = newConnection != null;
        if (check) {
            connections.put(newConnection);
            LOGGER.log(Level.ERROR, "Connection was lost while returning but replaced by a new one.");
        }
        return check;
    }

    private void returnBadConnection(WrappedConnection connection) throws InterruptedException {
        if (!fixConnection()) {
            connections.put(connection);
            LOGGER.log(Level.ERROR, "Connection can't be replaced");
        }
    }

    public void closeConnection() throws SQLException {
        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                connections.take().close();
            } catch (InterruptedException e) {
                LOGGER.log(Level.ERROR, e.getMessage());
            }
        }
        try {
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.ERROR, e + " Driver problem or driver no found");
        }
    }
}
