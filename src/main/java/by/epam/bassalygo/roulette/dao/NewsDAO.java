package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.News;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * Extension of AbstractDAO class for News entity
 *
 * @see AbstractDAO
 */
public class NewsDAO extends AbstractDAO<News> {
    /**
     * SQL query which selects all news from table
     */
    private static final String SQL_SELECT_ALL_NEWS = "SELECT newsId, head, text, img_path FROM news";
    /**
     * SQL query which insert new news
     */
    private static final String SQL_INSERT_NEWS = "INSERT INTO news(head, text, img_path) VALUES (?,?,?)";
    /**
     * SQL query which finds news by id
     */
    private static final String SQL_FIND_NEWS = "SELECT newsId, head, text, img_path FROM news WHERE newsId=?";
    /**
     * SQL query which update news
     */
    private static final String SQL_UPDATE_NEWS = "UPDATE news SET head=?, text=?, img_path=? WHERE newsId=?";
    /**
     * SQL query which delete news by id
     */
    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE newsId=?";

    /**
     * Default path of news images
     */
    private static final String DEFAULT_IMG_PATH = "E:\\opt\\Tomcat8\\webapps\\images\\roulette\\newsdef.PNG";

    public NewsDAO(WrappedConnection connection) {
        super(connection);
    }

    /**
     * Builds News object
     * @param resultSet object
     * @return news object
     * @throws SQLException
     */
    private News buildNews(ResultSet resultSet) throws SQLException {

        News news1 = new News();
        news1.setNewsId(resultSet.getInt("newsId"));
        news1.setHead(resultSet.getString("head"));
        news1.setText(resultSet.getString("text"));
        news1.setImgPath(resultSet.getString("img_path"));
        return news1;
    }

    /**
     * Finds all news
     * @return list of News objects
     * @throws RouletteDAOException
     */
    @Override
    public List<News> findAll() throws RouletteDAOException {
        List<News> news = null;
        Statement statement = null;
        try {
            news = new ArrayList<>();
            statement = connection.getStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_NEWS);
            while (resultSet.next()) {
                news.add(buildNews(resultSet));
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in NewsDAO findAll " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return news;
    }

    /**
     * Finds news by id
     * @param id used to find entity
     * @return news object
     * @throws RouletteDAOException
     */
    @Override
    public News findById(int id) throws RouletteDAOException {
        News news = null;
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SQL_FIND_NEWS);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                news = buildNews(resultSet);
            }
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in NewsDAO findById " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
        return news;
    }

    /**
     * Deletes news
     * @param id used to delete entity
     * @return status of delete operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean delete(int id) throws RouletteDAOException {
        try {
            PreparedStatement statement = connection.getPreparedStatement(SQL_DELETE_NEWS);
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in NewsDAO delete " + e.getMessage());
        }
    }

    /**
     * Creates news
     * @param entity which needed to create
     * @return status of create operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean create(News entity) throws RouletteDAOException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(SQL_INSERT_NEWS);
            statement.setString(1, entity.getHead());
            statement.setString(2, entity.getText());
            if (entity.getImgPath() != null || !entity.getImgPath().equals(""))  {
                statement.setString(3, entity.getImgPath());
            } else {
                statement.setString(3, DEFAULT_IMG_PATH);
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in NewsDAO create " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }

    /**
     * Updates news
     * @param entity which needed to update
     * @return status of update operation (true or false)
     * @throws RouletteDAOException
     */
    @Override
    public boolean update(News entity) throws RouletteDAOException {

        PreparedStatement statement = null;

        LOGGER.log(Level.INFO, entity.toString());
        try {
            statement = connection.getPreparedStatement(SQL_UPDATE_NEWS);
            if (entity.getHead() != null) {
                statement.setString(1, entity.getHead());
            }
            if (entity.getText() != null) {
                statement.setString(2, entity.getText());
            }
            if (entity.getImgPath() != null) {
                statement.setString(3, entity.getImgPath());
            } else {
                statement.setString(3, DEFAULT_IMG_PATH);
            }
            if (entity.getNewsId() != 0) {
                statement.setInt(4, entity.getNewsId());
            }
            return statement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new RouletteDAOException("SQL exception in NewsDAO update " + e.getMessage());
        } finally {
            connection.closeStatement(statement);
        }
    }
}
