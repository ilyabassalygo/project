package by.epam.bassalygo.roulette.servlet.impl.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUtil {
    public static int getUserId(HttpServletRequest request) {
        return (int) request.getSession().getAttribute("user.userId");
    }


    /**
     * parse id from request
     *
     * @param request
     * @return id
     */
    public static String getIdFromParameter(HttpServletRequest request) {
        Enumeration<String> names = request.getParameterNames();
        String id = null;
        String idPattern = "\\d+";
        Pattern pattern = Pattern.compile(idPattern);

        while (names.hasMoreElements()) {
            String element = names.nextElement();
            Matcher matcher = pattern.matcher(element);
            if (matcher.matches()) {
                id = element;
            }
        }
        return id;
    }

    /**
     * Clears message attr
     * @param request
     */
    public static void clearMessageAttrs(HttpServletRequest request) {
        request.setAttribute("message", "");
    }
}
