package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.MessageService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides message handling
 */
public class TextMessageCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return text_message.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        CommandUtil.clearMessageAttrs(request);
        String text = request.getParameter("message_text");
        User user = (User) request.getSession().getAttribute("user");
        MessageService messageService = new MessageService();
        if (messageService.writeMessage(text, user.getUserId())) {
            request.getSession().setAttribute("userMessageList", messageService.findMessagesByUser(user.getUserId()));
            request.getSession().setAttribute("messageList", messageService.findAllMessages());
            request.setAttribute("message", "your message successfully sended");
        } else {
            request.setAttribute("message", "can't send message, try later");
        }
        return PathManager.getProperty("path.text_message");
    }
}
