package by.epam.bassalygo.roulette.servlet;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.NewsDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.entity.News;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@WebServlet(name = "UploadFile", urlPatterns = "/upload",
        initParams = {
                @WebInitParam(name = "file-upload", value = "E:\\opt\\Tomcat8\\webapps\\images\\roulette"),
        })
@MultipartConfig(location = "E:\\opt\\Tomcat8\\webapps\\images\\roulette\\", maxFileSize = 10485760L)
public class UploadServlet extends HttpServlet {
    public static final Logger LOGGER = LogManager.getRootLogger();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MultipartMap map = new MultipartMap(request, this);
        String check = map.getParameter("upload_picture");
        File file = map.getFile("file");

        ServletConfig config = getServletConfig();
        final String UPLOAD_DIRECTORY = config.getInitParameter("file-upload");
        if (ServletFileUpload.isMultipartContent(request)) {
            try (DAOHelper daoHelper = new DAOHelper()) {

                switch (check) {
                    case "avatar":
                        UserDAO userDAO = daoHelper.getUserDAO();
                        User user = (User) request.getSession().getAttribute("user");
                        request.getSession().setAttribute("img_path", UPLOAD_DIRECTORY + File.separator + file.getName());
                        user.setImgPath(UPLOAD_DIRECTORY + File.separator + file.getName());
                        userDAO.update(user);
                        request.getRequestDispatcher(PathManager.getProperty("path.profile")).forward(request, response);
                        break;
                    case "news":
                        int id = Integer.parseInt(map.getParameter("news_id"));
                        NewsDAO newsDAO = daoHelper.getNewsDAO();
                        News news = newsDAO.findById(id);
                        news.setImgPath(UPLOAD_DIRECTORY + File.separator + file.getName());
                        newsDAO.update(news);
                        request.getRequestDispatcher(PathManager.getProperty("path.news_edit")).forward(request, response);
                        break;
                    default:
                        throw new RouletteException("Missed case in file upload");
                }
                request.setAttribute("message", "File Uploaded Successfully");
            } catch (RouletteException ex) {
                LOGGER.log(Level.INFO, ex.getMessage());
            } catch (Exception ex) {
                request.setAttribute("message", "File Upload Failed, choose right file ");
            }
        } else {
            request.setAttribute("message",
                    "Sorry this Servlet only handles file upload request");
        }
    }

}
