package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.UserHasBanService;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides unban of user
 */
public class UnbanUserCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return users_info.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.valueOf(CommandUtil.getIdFromParameter(request));
        UserHasBanService userHasBanService = new UserHasBanService();
        if (!userHasBanService.unbanUser(id)) {
            request.setAttribute("message", "error was occured while unban");
        } else {
            request.getServletContext().setAttribute("bannedUsers", userHasBanService.findBannedUsers());
            request.getServletContext().setAttribute("users", UserService.findActiveUsers());
        }
        return PathManager.getProperty("path.users_info");
    }
}
