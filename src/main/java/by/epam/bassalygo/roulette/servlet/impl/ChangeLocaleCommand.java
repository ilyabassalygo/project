package by.epam.bassalygo.roulette.servlet.impl;


import by.epam.bassalygo.roulette.resource.PageContentManager;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeLocaleCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String locale = request.getParameter("locale");
        String path = request.getParameter("path");
        PageContentManager.chooseBundle(locale);
        request.setAttribute("heh", PageContentManager.getProperty("label.back"));
        request.getSession().setAttribute("locale", locale);
        return path;
    }
}
