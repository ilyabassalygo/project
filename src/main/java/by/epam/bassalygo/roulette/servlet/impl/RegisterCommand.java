package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.validation.RegistrationValidator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides registration of user
 */
public class RegisterCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp or register.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        RegistrationValidator validator = new RegistrationValidator();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String checkPassword = request.getParameter("password-check");
        String date = request.getParameter("bday");
        String email = request.getParameter("email");
        StringBuilder errorMessage = new StringBuilder();

        User user = new User();

        //validate input parameters
        if (validator.validate(login, password, checkPassword, date, email, errorMessage)) {
            UserService userService = new UserService();
            //try to register
            if (userService.registerUser(user, login, password, name, date, email, errorMessage)) {
                request.getSession().setAttribute("user", user);
                CreditService creditService = new CreditService();
                request.getSession().setAttribute("creditType", creditService.findAvailableCreditTypes(user.getUserId()));
                return PathManager.getProperty("path.profile");
            } else {
                request.setAttribute("error_register", errorMessage);
                return PathManager.getProperty("path.register");
            }
        } else {
            request.setAttribute("error_register", errorMessage.toString());
            return PathManager.getProperty("path.register");
        }
    }
}
