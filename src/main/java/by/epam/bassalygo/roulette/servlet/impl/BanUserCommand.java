package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.BanService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * Prepare request for banning user
 */
public class BanUserCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return ban_edit.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String id = CommandUtil.getIdFromParameter(request);
        BanService banService = new BanService();
        request.getServletContext().setAttribute("banType", banService.findAllBans());

        request.setAttribute("message", "");
        request.getSession().setAttribute("banDate", LocalDateTime.now());
        request.getSession().setAttribute("banId", id);
        return PathManager.getProperty("path.ban_edit");
    }
}
