package by.epam.bassalygo.roulette.servlet.filter;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filter all queries to /jsp/user/*
 */
@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE
}, urlPatterns = {"/jsp/user/*", "/jsp/admin/*", "/index.jsp"})
public class RoleFilter implements Filter {
    private static final Logger LOGGER = LogManager.getRootLogger();

    private enum UserRole {
        GUEST, BANNED
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();

        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            user = new User();
        }
        if (user.getRole() == null) {
            String role = UserRole.GUEST.toString().toLowerCase();
            user.setRole(role);
            session.setAttribute("user", user);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher(PathManager.getProperty("path.hello"));
            dispatcher.forward(req, resp);
            return;
        }
        if (user.getRole().equals(UserRole.BANNED.toString().toLowerCase())) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher(PathManager.getProperty("path.banned"));
            dispatcher.forward(req, resp);
            return;
        }
// pass the request along the filter chain
        chain.doFilter(request, response);
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }
}
