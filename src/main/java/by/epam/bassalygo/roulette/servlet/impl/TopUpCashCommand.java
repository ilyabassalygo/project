package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * Provides top up cash operation
 */
public class TopUpCashCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = new UserService();
        User user = (User) request.getSession().getAttribute("user");
        BigDecimal cashIncrease = new BigDecimal(request.getParameter("cash_increase_value"));
        userService.topUpCash(user, cashIncrease);
        return PathManager.getProperty("path.profile");
    }
}
