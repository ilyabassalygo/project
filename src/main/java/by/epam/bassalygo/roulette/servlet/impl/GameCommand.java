package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.BetService;
import by.epam.bassalygo.roulette.parser.BetParser;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Provides preparing of game.jsp
 */
public class GameCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return game.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BetService betService = new BetService();
        BetParser betParser = new BetParser();
        List<Bet> betList = betParser.parseBets(request);
        User user = (User) request.getSession().getAttribute("user");
        betService.clearCash(betList, user);
        request.getSession().setAttribute("user", user);
        return PathManager.getProperty("path.game");
    }
}
