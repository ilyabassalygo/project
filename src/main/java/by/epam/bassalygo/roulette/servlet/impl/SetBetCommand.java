package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.parser.BetParser;
import by.epam.bassalygo.roulette.service.BetService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides adding bet to active bet list
 */
public class SetBetCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return game.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BetService betService = new BetService();
        BetParser betParser = new BetParser();
        String type = request.getParameter("type");
        String cell = request.getParameter("cell");
        String size = request.getParameter("size");
        User user = (User) request.getSession().getAttribute("user");
        List<Bet> bets = betParser.parseBets(request);
        if (bets == null) {
            bets = new ArrayList<>();
        }
        if (betService.addBet(bets, type, cell, size, user)) {
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("bets", bets);
        } else {
            request.setAttribute("message", "you have not enough money");
        }
        return PathManager.getProperty("path.game");
    }
}
