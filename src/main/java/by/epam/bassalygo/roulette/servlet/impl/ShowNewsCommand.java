package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.NewsService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Posts news on jsp page
 */
public class ShowNewsCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return hello.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        NewsService newsService = new NewsService();
        request.getServletContext().setAttribute("news", newsService.findNews());
        return PathManager.getProperty("path.hello");
    }
}
