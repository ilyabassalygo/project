package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides navigation through jsp
 */
public class NavigationCommand implements ActionCommand {

    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return request.getParameter("path");
    }
}
