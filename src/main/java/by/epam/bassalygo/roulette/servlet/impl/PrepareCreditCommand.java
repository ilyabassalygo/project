package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.dao.CreditDAO;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import org.apache.logging.log4j.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;

public class PrepareCreditCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return take_credit.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        try (DAOHelper daoHelper = new DAOHelper()) {
            User user = (User) request.getSession().getAttribute("user");
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            Credit creditType = creditDAO.findCreditByType(request.getParameter("credit_type"));
            UserHasCredit credit = new UserHasCredit();

            credit.setUserId(user.getUserId());
            credit.setCreditId(creditType.getCreditId());

            LocalDate now = LocalDate.now();
            credit.setTakingDate(Date.valueOf(now));
            now = now.plusDays(creditType.getPaymentDays());
            credit.setRepaymentDate(Date.valueOf(now));
            credit.setCanceled(false);
            request.getSession().setAttribute("credit", credit);
            request.getSession().setAttribute("credType", creditType);

        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }

        return PathManager.getProperty("path.take_credit");
    }
}
