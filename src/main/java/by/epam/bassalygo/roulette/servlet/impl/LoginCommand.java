package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteException;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.service.UserHasBanService;
import by.epam.bassalygo.roulette.service.UserHasCreditService;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import org.apache.logging.log4j.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Provides login of user
 */
public class LoginCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp or login.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = new UserService();
        User user = new User();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        try {
            if (userService.checkLogin(user, login, password)) {
                request.getSession().setAttribute("user", user);
                UserHasBanService userHasBanService = new UserHasBanService();
                //check is user banned
                userHasBanService.checkBan(user);
                UserHasCreditService creditService = new UserHasCreditService();

                //shows credit info
                CreditService credit = new CreditService();
                List<Credit> creditTypes = credit.findAllCreditTypes();
                request.getSession().setAttribute("credittypes", creditTypes);
                //set user credits
                List<Credit> creditList = credit.findAvailableCreditTypes(user.getUserId());

                //set available credits for user
                request.getSession().setAttribute("creditType", creditList);
                //set user active credits
                request.getSession().setAttribute("credits", creditService.findCredits(user.getUserId()));

                return PathManager.getProperty("path.profile");
            } else {
                request.setAttribute("message", "Wrong login or password");
                return PathManager.getProperty("path.login");
            }
        } catch (RouletteException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
            return PathManager.getProperty("path.login");
        }
    }
}
