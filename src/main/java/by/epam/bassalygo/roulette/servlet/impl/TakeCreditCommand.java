package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.service.UserHasCreditService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * Provides operation of taking credit
 */
public class TakeCreditCommand implements ActionCommand {

    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserHasCreditService creditService = new UserHasCreditService();
        CreditService creditTypes = new CreditService();
        User user = (User) request.getSession().getAttribute("user");
        Credit creditType = (Credit) request.getSession().getAttribute("credType");
        UserHasCredit credit = (UserHasCredit) request.getSession().getAttribute("credit");
        BigDecimal payment = new BigDecimal(request.getParameter("credit_summ"));
        StringBuilder message = new StringBuilder();

        if (creditService.takeCredit(user, creditType, credit, payment, message)) {
            request.getSession().setAttribute("user", user);
            request.setAttribute("message", message);
            request.getSession().setAttribute("credits", creditService.findCredits(user.getUserId()));
            request.getSession().setAttribute("creditType", creditTypes.findAvailableCreditTypes(user.getUserId()) );
        } else {
            request.setAttribute("message", message);
        }
        creditService.findCredits(user.getUserId());

        return PathManager.getProperty("path.profile");
    }
}
