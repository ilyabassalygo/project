package by.epam.bassalygo.roulette.servlet;

import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.*;

public enum CommandEnum {
    CHANGE_LOCALE {
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    GAME {
        {
            this.command = new GameCommand();
        }
    },
    NEWS {
        {
            this.command = new NewsCommand();
        }
    },
    SHOW_NEWS {
        {
            this.command = new ShowNewsCommand();
        }
    },
    ADD_NEWS {
        {
            this.command = new AddNewsCommand();
        }
    },
    EDIT_NEWS {
        {
            this.command = new EditNewsCommand();
        }
    },
    DELETE_NEWS {
        {
            this.command = new DeleteNewsCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    EDIT_PROFILE {
        {
            this.command = new EditProfileCommand();
        }
    },
    TOP_UP_CASH {
        {
            this.command = new TopUpCashCommand();
        }
    },
    CREDIT_INFO {
        {
            this.command = new CreditInfoCommand();
        }
    },
    PREPARE_CREDIT {
        {
            this.command = new PrepareCreditCommand();
        }
    },
    TAKE_CREDIT {
        {
            this.command = new TakeCreditCommand();
        }
    },
    CREDIT_PAYMENT {
        {
            this.command = new CreditPaymentCommand();
        }
    },
    NAVIGATION {
        {
            this.command = new NavigationCommand();
        }
    },
    PROFILE {
        {
            this.command = new ProfileCommand();
        }
    },
    STATISTICS {
        {
            this.command = new StatisticsCommand();
        }
    },
    BAN {
        {
            this.command = new BanCommand();
        }
    },
    BAN_USER {
        {
            this.command = new BanUserCommand();
        }
    },
    UNBAN_USER {
        {
            this.command = new UnbanUserCommand();
        }
    },
    SET_BET {
        {
            this.command = new SetBetCommand();
        }
    },
    DEL_BET {
        {
            this.command = new DelBetCommand();
        }
    },
    SPIN {
        {
            this.command = new SpinCommand();
        }
    },
    USERS_INFO {
        {
            this.command = new UsersInfoCommand();
        }
    },
    TEXT_MESSAGE {
        {
            this.command = new TextMessageCommand();
        }
    },
    MESSAGES {
        {
            this.command = new MessagesCommand();
        }
    },
    SHOW_USER_MESSAGES {
        {
            this.command = new ShowUserMessagesCommand();
        }
    },
    EDIT_MESSAGES {
        {
            this.command = new EditMessages();
        }
    },
    DELETE_MESSAGES {
        {
            this.command = new DeleteMessages();
        }
    };
    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
