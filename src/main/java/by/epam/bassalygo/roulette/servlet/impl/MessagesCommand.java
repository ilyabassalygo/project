package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.service.MessageService;
import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Posts all messages
 */
public class MessagesCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return messages.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        CommandUtil.clearMessageAttrs(request);
        MessageService messageService = new MessageService();
        request.getSession().setAttribute("messageList", messageService.findAllMessages());
        return PathManager.getProperty("path.messages");
    }
}
