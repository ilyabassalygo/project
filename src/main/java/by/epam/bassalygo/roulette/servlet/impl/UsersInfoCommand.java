package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.service.UserHasBanService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Posts banned and active users on jsp page
 */
public class UsersInfoCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return users_info.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserHasBanService userHasBanService = new UserHasBanService();
        request.getServletContext().setAttribute("bannedUsers", userHasBanService.findBannedUsers());
        request.getServletContext().setAttribute("users", UserService.findActiveUsers());
        return PathManager.getProperty("path.users_info");
    }

}
