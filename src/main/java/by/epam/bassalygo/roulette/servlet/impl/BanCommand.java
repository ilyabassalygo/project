package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.UserHasBanService;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides ban of user
 */
public class BanCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return users_info.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserHasBanService userHasBanService = new UserHasBanService();
        int banId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("banId")));
        String type = request.getParameter("type");
        String date = String.valueOf(request.getSession().getAttribute("banDate"));
        String reason = request.getParameter("reason");
        if (!userHasBanService.banUser(banId, type, date, reason)) {
            request.setAttribute("message", "can't ban user");
            return PathManager.getProperty("path.ban_edit");
        }
        request.getServletContext().setAttribute("users", UserService.findActiveUsers());
        request.getServletContext().setAttribute("bannedUsers", userHasBanService.findBannedUsers());
        request.setAttribute("message", "User " + request.getSession().getAttribute("banId") + " successfully banned");
        return PathManager.getProperty("path.users_info");
    }
}
