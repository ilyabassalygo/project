package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Posts info about available credits
 */
public class CreditInfoCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        CreditService creditService = new CreditService();
        request.setAttribute("check", "true");
        request.setAttribute("creditInfo", creditService.findAllCreditTypes());

        return PathManager.getProperty("path.profile");
    }
}
