package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.service.UserHasCreditService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreditPaymentCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserHasCreditService userHasCreditService = new UserHasCreditService();
        CreditService creditTypes = new CreditService();
        User user = (User) request.getSession().getAttribute("user");
        String[] payments = request.getParameterValues("credit_id");
        String[] amounts = request.getParameterValues("credit_payment_amount");
        StringBuilder message = new StringBuilder();

        if (userHasCreditService.creditPayment(payments, amounts, user, message)) {
            request.setAttribute("message", message);
            request.getSession().setAttribute("credits", userHasCreditService.findCredits(user.getUserId()));
            request.getSession().setAttribute("creditType", creditTypes.findAvailableCreditTypes(user.getUserId()) );
            request.getSession().setAttribute("user", user);
        } else {
            request.setAttribute("message", message);
        }

        return PathManager.getProperty("path.profile");
    }
}
