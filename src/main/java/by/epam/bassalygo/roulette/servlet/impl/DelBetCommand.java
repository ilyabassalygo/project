package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.parser.BetParser;
import by.epam.bassalygo.roulette.service.BetService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides deleting of bet from active bet list
 */
public class DelBetCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return game.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        BetService betService = new BetService();
        BetParser betParser = new BetParser();
        User user = (User) request.getSession().getAttribute("user");
        int index = Integer.parseInt(request.getParameter("index"));
        List<Bet> bets = betParser.parseBets(request);
        if (bets == null) {
            bets = new ArrayList<>();
        }
        if (!betService.removeBet(bets, user, index)) {
            request.setAttribute("message", "cant remove bet");
        }
        request.getSession().setAttribute("user", user);
        request.getSession().setAttribute("bets", bets);
        return PathManager.getProperty("path.game");
    }
}
