package by.epam.bassalygo.roulette.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ActionCommand {
    Logger LOGGER = LogManager.getRootLogger();

    String execute(HttpServletRequest request, HttpServletResponse response);
}
