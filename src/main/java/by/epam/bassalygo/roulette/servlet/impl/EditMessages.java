package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.MessageService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;
import by.epam.bassalygo.roulette.servlet.impl.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides edit messages tools
 */
public class EditMessages implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return messages.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        MessageService messageService = new MessageService();
        String reply = request.getParameter("reply");
        if (messageService.editMessage(reply, CommandUtil.getIdFromParameter(request))) {
            request.getSession().setAttribute("messageList", messageService.findAllMessages());
        } else {
            request.setAttribute("message", "error was occured while edit");
        }
        return PathManager.getProperty("path.messages");
    }
}
