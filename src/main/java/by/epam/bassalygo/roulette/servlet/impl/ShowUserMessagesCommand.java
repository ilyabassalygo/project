package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.MessageService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Posts messages by user on jsp page
 */
public class ShowUserMessagesCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return reply.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        MessageService messageService = new MessageService();
        User user = (User) request.getSession().getAttribute("user");
        request.getSession().setAttribute("userMessageList", messageService.findMessagesByUser(user.getUserId()));
        return PathManager.getProperty("path.reply");
    }
}
