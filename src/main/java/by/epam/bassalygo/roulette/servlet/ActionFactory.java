package by.epam.bassalygo.roulette.servlet;

import by.epam.bassalygo.roulette.servlet.impl.EmptyCommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class ActionFactory {
    public ActionCommand defineCommand(HttpServletRequest request) throws IOException, ServletException {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());

        current = currentEnum.getCurrentCommand();

        return current;
    }
}