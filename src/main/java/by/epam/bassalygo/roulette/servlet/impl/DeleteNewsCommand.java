package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.News;
import by.epam.bassalygo.roulette.service.NewsService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class DeleteNewsCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return news_edit.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        NewsService newsService = new NewsService();
        List<News> newsList = (List<News>) request.getServletContext().getAttribute("news");
        int index = Integer.parseInt(request.getParameter("index"));
        newsService.deleteNews(index, newsList);
        request.getServletContext().setAttribute("news", newsService.findNews());
        return PathManager.getProperty("path.news_edit");
    }
}
