package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides creation of statistics
 */
public class StatisticsCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return statistics.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = new UserService();
        userService.createStatistics(request);
        return PathManager.getProperty("path.statistics");
    }
}
