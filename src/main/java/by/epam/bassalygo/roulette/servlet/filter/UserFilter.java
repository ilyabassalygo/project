package by.epam.bassalygo.roulette.servlet.filter;

import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.resource.PathManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter all queries to /jsp/admin/*
 */
@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE
}, urlPatterns = {"/jsp/user/*"})
public class UserFilter implements Filter {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private static final String USER = "user";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        User user = (User) req.getSession().getAttribute("user");
        if (!user.getRole().equals(USER)) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher(PathManager.getProperty("path.hello"));
            dispatcher.forward(req, resp);
            return;
        }
// pass the request along the filter chain
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
