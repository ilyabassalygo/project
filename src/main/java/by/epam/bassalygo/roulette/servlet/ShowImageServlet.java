package by.epam.bassalygo.roulette.servlet;

import by.epam.bassalygo.roulette.dao.NewsDAO;
import by.epam.bassalygo.roulette.entity.News;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(urlPatterns = "/show")
public class ShowImageServlet extends HttpServlet {
    private final static Logger LOGGER = LogManager.getRootLogger();
    private static final String CONTENT_TYPE_REG = "(jpg|JPG|jpeg|JPEG|png|PNG)";

    private void show(String imgPath, HttpServletResponse response) throws IOException {

        FileInputStream fis;
        BufferedInputStream bis;
        BufferedOutputStream output;

        Pattern contentTypePattern = Pattern.compile(CONTENT_TYPE_REG);
        Matcher contentTypeMatcher = contentTypePattern.matcher(imgPath);

        fis = new FileInputStream(new File(imgPath));
        bis = new BufferedInputStream(fis);

        if (contentTypeMatcher.find()) {
            String extension = contentTypeMatcher.group();
            response.setContentType("image/" + extension.toLowerCase());
        }

        output = new BufferedOutputStream(response.getOutputStream());
        for (int data; (data = bis.read()) > -1; ) {
            output.write(data);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");
        String imgPath;
        User user = (User) request.getSession().getAttribute("user");
        try (DAOHelper daoHelper = new DAOHelper()) {
            switch (type) {
                case "avatar":
                    imgPath = user.getImgPath();

                    this.show(imgPath, response);
                    break;
                case "news":
                    NewsDAO newsDAO = daoHelper.getNewsDAO();
                    News news = newsDAO.findById(Integer.parseInt(request.getParameter("id")));
                    imgPath = news.getImgPath();

                    this.show(imgPath, response);
                    break;
                default:
                    throw new RouletteException("Bad case type");
            }


        } catch (FileNotFoundException e) {
            LOGGER.log(Level.INFO, "Cant find image path " + e);
        } catch (IOException e) {
            LOGGER.log(Level.INFO, "Cant read image " + e);
        } catch (RouletteException e) {
            LOGGER.log(Level.INFO, e);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }
}
