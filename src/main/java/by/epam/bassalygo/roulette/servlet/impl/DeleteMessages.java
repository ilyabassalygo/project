package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.MessageService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides deleting of messages
 */
public class DeleteMessages implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return messages.jsp or reply.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        String id = request.getParameter("mes_id");
        String deleted = request.getParameter("deleted");
        MessageService messageService = new MessageService();
        if (!messageService.deleteMessage(id, deleted)) {
            request.setAttribute("message", "error was occured while delete");
        }
        if (user.getRole().equals("user")) {
            request.getSession().setAttribute("userMessageList", messageService.findMessagesByUser(user.getUserId()));
            return PathManager.getProperty("path.reply");
        } else {
            request.getSession().setAttribute("messageList", messageService.findAllMessages());
            return PathManager.getProperty("path.messages");
        }
    }
}
