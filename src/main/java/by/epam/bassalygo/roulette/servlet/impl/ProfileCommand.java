package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.CreditService;
import by.epam.bassalygo.roulette.service.UserHasCreditService;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides user info in profile
 */
public class ProfileCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return profile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = new UserService();
        UserHasCreditService userHasCreditService = new UserHasCreditService();
        CreditService creditTypes = new CreditService();
        User user = (User) request.getSession().getAttribute("user");
        request.getSession().setAttribute("user", userService.findUser(user.getUserId()));
        request.getSession().setAttribute("credits", userHasCreditService.findCredits(user.getUserId()));
        request.getSession().setAttribute("creditType", creditTypes.findAvailableCreditTypes(user.getUserId()));

        return PathManager.getProperty("path.profile");
    }
}
