package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.entity.Id;
import by.epam.bassalygo.roulette.entity.Spin;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.parser.BetParser;
import by.epam.bassalygo.roulette.service.SpinService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * Provides spin handling
 */
public class SpinCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return game.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        SpinService spinService = new SpinService();

        User user = (User) request.getSession().getAttribute("user");

        BetParser betParser = new BetParser();
        List<Bet> bets = betParser.parseBets(request);

        final double CELLS_NUMBER = 37;
        int winCell = (int) (Math.random() * CELLS_NUMBER);

        BigDecimal totalWin = new BigDecimal("0");

        Spin spin = new Spin(Id.generateId(), winCell, totalWin,
                Date.valueOf(LocalDate.now()), user.getUserId());

        spinService.spin(spin, bets, user, winCell, totalWin);

        request.getSession().setAttribute("user", user);
        request.setAttribute("previosBets", bets);
        request.setAttribute("win_cell", winCell);
        request.setAttribute("total_win", spin.getWinAmount());
        request.getSession().setAttribute("bets", null);
        return PathManager.getProperty("path.game");
    }
}
