package by.epam.bassalygo.roulette.servlet.impl;

import by.epam.bassalygo.roulette.resource.PathManager;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.service.UserService;
import by.epam.bassalygo.roulette.servlet.ActionCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides editing of profile
 */
public class EditProfileCommand implements ActionCommand {
    /**
     * Execute command
     *
     * @param request
     * @param response
     * @return editprofile.jsp path
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        UserService userService = new UserService();
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String bday = request.getParameter("bday");
        User user = (User) request.getSession().getAttribute("user");
        if (userService.editProfile(name, email, bday, user)) {
            return PathManager.getProperty("path.profile");
        }
        return PathManager.getProperty("path.editprofile");
    }
}
