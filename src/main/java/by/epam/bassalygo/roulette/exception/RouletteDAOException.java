package by.epam.bassalygo.roulette.exception;

public class RouletteDAOException extends Exception {
    public RouletteDAOException() {
    }

    public RouletteDAOException(String message) {
        super(message);
    }

    public RouletteDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public RouletteDAOException(Throwable cause) {
        super(cause);
    }
}
