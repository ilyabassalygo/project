package by.epam.bassalygo.roulette.exception;

public class RouletteException extends Exception {
    public RouletteException() {
    }

    public RouletteException(String message) {
        super(message);
    }

    public RouletteException(Throwable cause) {
        super(cause);
    }

    public RouletteException(String message, Throwable cause) {
        super(message, cause);
    }
}
