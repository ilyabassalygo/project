package by.epam.bassalygo.roulette.entity;

import java.sql.Timestamp;

/**
 * This class represent message entity in database
 */
public class Message {
    /**
     * Id of message
     */
    private int messageId;
    /**
     * Message text
     */
    private String text;
    /**
     * Date when message where posted
     */
    private Timestamp date;
    /**
     * Id of user who sent message
     */
    private int userId;
    /**
     * Reply to message
     */
    private String reply;
    /**
     * Flag to check if user delete message
     */
    private boolean deleted;
    /**
     * Object of user who sent message
     */
    private User user;

    public Message() {
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageId=" + messageId +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", userId=" + userId +
                ", reply='" + reply + '\'' +
                ", deleted=" + deleted +
                ", user=" + user +
                '}';
    }
}
