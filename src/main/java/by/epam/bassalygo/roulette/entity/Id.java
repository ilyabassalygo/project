package by.epam.bassalygo.roulette.entity;

import by.epam.bassalygo.roulette.dao.SpinDAO;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.dao.DAOHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Generates id for spin entity
 */
public class Id {
    private static final Logger LOGGER = LogManager.getRootLogger();
    /**
     * id
     */
    private static int id = 0;

    static {
        try (DAOHelper daoHelper = new DAOHelper()) {
            SpinDAO spinDAO = daoHelper.getSpinDAO();
            id = spinDAO.findAll().size();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    /**
     * Generates id
     *
     * @return generated id
     */
    public static int generateId() {
        return ++id;
    }
}
