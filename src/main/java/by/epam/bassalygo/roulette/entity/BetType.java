package by.epam.bassalygo.roulette.entity;

/**
 * This class represent bet type entity in database
 */
public class BetType {
    /**
     * Id of bet
     */
    private int betId;
    /**
     * Name of bet type
     */
    private String name;
    /**
     * Payment coefficient for this bet type
     */
    private int payment;

    public BetType() {
    }

    public BetType(int betId, String name, int payment) {
        this.betId = betId;
        this.name = name;
        this.payment = payment;
    }

    public int getBetId() {
        return betId;
    }

    public void setBetId(int betId) {
        this.betId = betId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BetType)) return false;

        BetType betType = (BetType) o;

        if (betId != betType.betId) return false;
        if (payment != betType.payment) return false;
        return name != null ? name.equals(betType.name) : betType.name == null;

    }

    @Override
    public int hashCode() {
        int result = betId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + payment;
        return result;
    }

    @Override
    public String toString() {
        return "BetType{" +
                "betId=" + betId +
                ", name='" + name + '\'' +
                ", payment=" + payment +
                '}';
    }
}
