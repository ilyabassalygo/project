package by.epam.bassalygo.roulette.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * This class represent spin entity in database
 */
public class Spin {
    /**
     * Id of spin
     */
    private int spinId;
    /**
     * Win cell number of spin
     */
    private int winCellNumber;
    /**
     * Amount of win of spin
     */
    private BigDecimal winAmount;
    /**
     * Date when spin was made
     */
    private Date date;
    /**
     * Id of user who made this spin
     */
    private int userId;

    public Spin() {
    }

    public Spin(int spinId, int winCellNumber, BigDecimal winAmount, Date date, int userId) {
        this.spinId = spinId;
        this.winCellNumber = winCellNumber;
        this.winAmount = winAmount;
        this.date = date;
        this.userId = userId;
    }

    public int getSpinId() {
        return spinId;
    }

    public void setSpinId(int spinId) {
        this.spinId = spinId;
    }

    public int getWinCellNumber() {
        return winCellNumber;
    }

    public void setWinCellNumber(int winCellNumber) {
        this.winCellNumber = winCellNumber;
    }

    public BigDecimal getWinAmount() {
        return winAmount;
    }

    public void setWinAmount(BigDecimal winAmount) {
        this.winAmount = winAmount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
