package by.epam.bassalygo.roulette.entity;

import java.math.BigDecimal;

/**
 * This class represent bet entity in database
 */
public class Bet {
    /**
     * Id of bet
     */
    private int betId;
    /**
     * Id of user who made this bet
     */
    private int userId;
    /**
     * Id of bet type
     */
    private int betTypeId;
    /**
     * Size of bet
     */
    private BigDecimal betSize;
    /**
     * Win size of bet
     */
    private BigDecimal winSize;
    /**
     * Cells which where chosen by user
     */
    private String cells;
    /**
     * Id of spin in which bet was made
     */
    private int spinId;

    private BetType betType;

    public Bet() {
        betType = new BetType();
    }

    public Bet(int userId, int betTypeId, BigDecimal betSize, BigDecimal winSize, String cells, int spinId) {
        this.userId = userId;
        this.betTypeId = betTypeId;
        this.betSize = betSize;
        this.winSize = winSize;
        this.cells = cells;
        this.spinId = spinId;
    }

    public int getBetId() {
        return betId;
    }

    public void setBetId(int betId) {
        this.betId = betId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBetTypeId() {
        return betTypeId;
    }

    public void setBetTypeId(int betTypeId) {
        this.betTypeId = betTypeId;
    }

    public BigDecimal getBetSize() {
        return betSize;
    }

    public void setBetSize(BigDecimal betSize) {
        this.betSize = betSize;
    }

    public BigDecimal getWinSize() {
        return winSize;
    }

    public void setWinSize(BigDecimal winSize) {
        this.winSize = winSize;
    }

    public String getCells() {
        return cells;
    }

    public void setCells(String cells) {
        this.cells = cells;
    }

    public int getSpinId() {
        return spinId;
    }

    public void setSpinId(int spinId) {
        this.spinId = spinId;
    }

    public BetType getBetType() {
        return betType;
    }

    public void setBetType(BetType betType) {
        this.betType = betType;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "betId=" + betId +
                ", userId=" + userId +
                ", betTypeId=" + betTypeId +
                ", betSize=" + betSize +
                ", winSize=" + winSize +
                ", cells='" + cells + '\'' +
                ", spinId=" + spinId +
                ", betType=" + betType.toString() +
                '}';
    }
}
