package by.epam.bassalygo.roulette.entity;

import java.sql.Time;

/**
 * This class represent ban entity in database
 */
public class Ban {
    /**
     * Id of ban
     */
    private int banId;
    /** Ban type */
    private String type;
    /** Duration of ban in days */
    private int days;
    /** Duration of ban in time */
    private java.sql.Time time;

    public Ban() {
    }

    public Ban(int banId, String type, int days, Time time) {
        this.banId = banId;
        this.type = type;
        this.days = days;
        this.time = time;
    }

    public int getBanId() {
        return banId;
    }

    public void setBanId(int banId) {
        this.banId = banId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public java.sql.Time getTime() {
        return time;
    }

    public void setTime(java.sql.Time time) {
        this.time = time;
    }
}
