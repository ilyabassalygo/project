package by.epam.bassalygo.roulette.entity;

import java.math.BigDecimal;

/**
 * This class represent credit entity in database
 */
public class Credit {
    /**
     * Id of credit
     */
    private int creditId;
    /**
     * Type of credit
     */
    private String type;
    /**
     * Maximum payment for this credit
     */
    private BigDecimal maxPayment;
    /**
     * Payment days for this credit type
     */
    private int paymentDays;
    /**
     * Percent of credit
     */
    private int percent;

    public Credit() {
    }

    public Credit(int creditId, String type, BigDecimal maxPayment, int paymentDays, int percent) {
        this.creditId = creditId;
        this.type = type;
        this.maxPayment = maxPayment;
        this.paymentDays = paymentDays;
        this.percent = percent;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getMaxPayment() {
        return maxPayment;
    }

    public void setMaxPayment(BigDecimal maxPayment) {
        this.maxPayment = maxPayment;
    }

    public int getPaymentDays() {
        return paymentDays;
    }

    public void setPaymentDays(int paymentDays) {
        this.paymentDays = paymentDays;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Credit)) return false;

        Credit credit = (Credit) o;

        if (creditId != credit.creditId) return false;
        if (paymentDays != credit.paymentDays) return false;
        if (percent != credit.percent) return false;
        if (type != null ? !type.equals(credit.type) : credit.type != null) return false;
        return maxPayment != null ? maxPayment.equals(credit.maxPayment) : credit.maxPayment == null;

    }

    @Override
    public int hashCode() {
        int result = creditId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (maxPayment != null ? maxPayment.hashCode() : 0);
        result = 31 * result + paymentDays;
        result = 31 * result + percent;
        return result;
    }

    @Override
    public String toString() {
        return "Credit{" +
                "creditId=" + creditId +
                ", type='" + type + '\'' +
                ", maxPayment=" + maxPayment +
                ", paymentDays=" + paymentDays +
                ", percent=" + percent +
                '}';
    }
}
