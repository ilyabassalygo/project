package by.epam.bassalygo.roulette.entity;

import java.math.BigDecimal;
import java.sql.Date;


/**
 * This class represent user entity in database
 */
public class User {
    /**
     * Id of user
     */
    private int userId;
    /**
     * User login
     */
    private String login;
    /**
     * User password hash
     */
    private String password;
    /**
     * User role
     */
    private String role;
    /**
     * User name
     */
    private String name;
    /**
     * Birthdate of user to verify his age
     */
    private Date birthDate;
    /**
     * Email of user
     */
    private String email;
    /**
     * User cash balance
     */
    private BigDecimal cash;
    /**
     * Path of user avatar
     */
    private String imgPath;

    public User() {
    }

    public User(int userId, String login, String password, String role, String name, Date birthDate, String imgPath, BigDecimal cash, String email) {
        this.userId = userId;
        this.login = login;
        this.password = password;
        this.role = role;
        this.name = name;
        this.birthDate = birthDate;
        this.imgPath = imgPath;
        this.cash = cash;
        this.email = email;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (userId != user.userId) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (role != null ? !role.equals(user.role) : user.role != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (birthDate != null ? !birthDate.equals(user.birthDate) : user.birthDate != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (cash != null ? !cash.equals(user.cash) : user.cash != null) return false;
        return imgPath != null ? imgPath.equals(user.imgPath) : user.imgPath == null;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (cash != null ? cash.hashCode() : 0);
        result = 31 * result + (imgPath != null ? imgPath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate.toString() +
                ", email='" + email + '\'' +
                ", cash=" + cash.toString() +
                ", imgPath='" + imgPath + '\'' +
                '}';
    }

    public enum UserRole {
        GUEST, USER, ADMIN, BANNED
    }
}
