package by.epam.bassalygo.roulette.entity;

import java.sql.Timestamp;


/**
 * This class represent user UserHasBan in database
 */
public class UserHasBan {
    /**
     * Id of user who was banned
     */
    private int userId;
    /**
     * Id of ban which user get
     */
    private int banId;
    /**
     * Date when user was banned
     */
    private Timestamp banDate;
    /**
     * Date when user will be unbanned
     */
    private Timestamp unbanDate;
    /**
     * Reason of ban
     */
    private String reason;
    /**
     * Object of ban
     */
    private Ban ban;
    /**
     * Object of user
     */
    private User user;

    public UserHasBan() {
    }

    public UserHasBan(int userId, int banId, Timestamp banDate, Timestamp unbanDate, String reason, Ban ban, User user) {
        this.userId = userId;
        this.banId = banId;
        this.banDate = banDate;
        this.unbanDate = unbanDate;
        this.reason = reason;
        this.ban = ban;
        this.user = user;
    }

    private UserHasBan buildUserHasBan(int banId, Timestamp banDate, Timestamp unbanDateTime, String reason, Ban ban) {
        UserHasBan userHasBan = new UserHasBan();
        userHasBan.setUserId(banId);
        userHasBan.setBanId(ban.getBanId());
        userHasBan.setBanDate(banDate);
        userHasBan.setUnbanDate(unbanDateTime);
        userHasBan.setReason(reason);
        return userHasBan;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBanId() {
        return banId;
    }

    public void setBanId(int banId) {
        this.banId = banId;
    }

    public Timestamp getBanDate() {
        return banDate;
    }

    public void setBanDate(Timestamp banDate) {
        this.banDate = banDate;
    }

    public Timestamp getUnbanDate() {
        return unbanDate;
    }

    public void setUnbanDate(Timestamp unbanDate) {
        this.unbanDate = unbanDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Ban getBan() {
        return ban;
    }

    public void setBan(Ban ban) {
        this.ban = ban;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserHasBan)) return false;

        UserHasBan that = (UserHasBan) o;

        if (userId != that.userId) return false;
        if (banId != that.banId) return false;
        if (banDate != null ? !banDate.equals(that.banDate) : that.banDate != null) return false;
        if (unbanDate != null ? !unbanDate.equals(that.unbanDate) : that.unbanDate != null) return false;
        return reason != null ? reason.equals(that.reason) : that.reason == null;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + banId;
        result = 31 * result + (banDate != null ? banDate.hashCode() : 0);
        result = 31 * result + (unbanDate != null ? unbanDate.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        return result;
    }
}
