package by.epam.bassalygo.roulette.entity;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * This class represent user UserHasBan in database
 */
public class UserHasCredit {
    /**
     * Id of user who has took credit
     */
    private int userId;
    /**
     * Id of credit which user took
     */
    private int creditId;
    /**
     * Date when user took credit
     */
    private Date takingDate;
    /**
     * Date when user should repay credit
     */
    private Date repaymentDate;
    /**
     * Amount of payment for this type of credit
     */
    private BigDecimal paymentAmount;
    /**
     * Amount of payment that user done
     */
    private BigDecimal payment;
    /**
     * Status of credit
     */
    private boolean canceled;

    private Credit credit;
    private User user;

    public UserHasCredit() {
    }

    public UserHasCredit(int userId, int creditId, Date takingDate, Date repaymentDate, BigDecimal paymentAmount, BigDecimal payment, boolean canceled) {
        this.userId = userId;
        this.creditId = creditId;
        this.takingDate = takingDate;
        this.repaymentDate = repaymentDate;
        this.paymentAmount = paymentAmount;
        this.payment = payment;
        this.canceled = canceled;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    public Date getTakingDate() {
        return takingDate;
    }

    public void setTakingDate(Date takingDate) {
        this.takingDate = takingDate;
    }

    public Date getRepaymentDate() {
        return repaymentDate;
    }

    public void setRepaymentDate(Date repaymentDate) {
        this.repaymentDate = repaymentDate;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }


    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserHasCredit)) return false;

        UserHasCredit that = (UserHasCredit) o;

        if (userId != that.userId) return false;
        if (creditId != that.creditId) return false;
        if (canceled != that.canceled) return false;
        if (takingDate != null ? !takingDate.equals(that.takingDate) : that.takingDate != null) return false;
        if (repaymentDate != null ? !repaymentDate.equals(that.repaymentDate) : that.repaymentDate != null)
            return false;
        if (paymentAmount != null ? !paymentAmount.equals(that.paymentAmount) : that.paymentAmount != null)
            return false;
        return payment != null ? payment.equals(that.payment) : that.payment == null;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + creditId;
        result = 31 * result + (takingDate != null ? takingDate.hashCode() : 0);
        result = 31 * result + (repaymentDate != null ? repaymentDate.hashCode() : 0);
        result = 31 * result + (paymentAmount != null ? paymentAmount.hashCode() : 0);
        result = 31 * result + (payment != null ? payment.hashCode() : 0);
        result = 31 * result + (canceled ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserHasCredit{" +
                "canceled=" + canceled +
                ", userId=" + userId +
                ", creditId=" + creditId +
                ", takingDate=" + takingDate +
                ", repaymentDate=" + repaymentDate +
                ", paymentAmount=" + paymentAmount +
                ", payment=" + payment +
                '}';
    }
}


