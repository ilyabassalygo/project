package by.epam.bassalygo.roulette.entity;

/**
 * This class represent news entity in database
 */
public class News {
    /**
     * Id of news
     */
    private int newsId;
    /**
     * News head
     */
    private String head;
    /**
     * News text
     */
    private String text;
    /**
     * Path of news image
     */
    private String imgPath;

    public News() {
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", head='" + head + '\'' +
                ", text='" + text + '\'' +
                ", imgPath='" + imgPath + '\'' +
                '}';
    }
}
