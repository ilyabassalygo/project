package by.epam.bassalygo.roulette.parser;

import by.epam.bassalygo.roulette.entity.Bet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class BetParser {
    public List<Bet> parseBets(HttpServletRequest request) {
        List<Bet> bets = (ArrayList<Bet>) request.getSession().getAttribute("bets");
        return bets;
    }
}
