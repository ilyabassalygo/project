package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import by.epam.bassalygo.roulette.exception.RouletteException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class UserServiceTest {
    private UserService userService;
    private static final Logger LOGGER = LogManager.getRootLogger();
    private User user;
    private User testUser;

    @Before
    public void init() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            userService = new UserService();
            UserDAO userDAO = daoHelper.getUserDAO();
            user = userDAO.findByLogin("testguy");
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }


    @After
    public void destroy() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            user.setName("test");
            user.setCash(new BigDecimal("5000"));
            userDAO.update(user);
            if (testUser != null) {
                testUser = userDAO.findByLogin(testUser.getLogin());
                userDAO.delete(testUser.getUserId());
                testUser = null;
            }
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void checkLoginTest() {
        try {
            String login = "testguy";
            String password = "11114444iI";
            Assert.assertTrue(userService.checkLogin(user, login, password));
        } catch (RouletteException e) {
            Assert.fail();
        }
    }

    @Test
    public void registerUserTest() {
        testUser = new User();
        String login = "testUser";
        String password = "11114444iI";
        Assert.assertTrue(userService.registerUser(testUser, login, password, "test", "1900-01-01", "mail", new StringBuilder()));
    }

    @Test
    public void editProfileTest() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            user.setName("newName");
            userService.editProfile(user.getName(), "", "", user);
            Assert.assertEquals(user.getName(), userDAO.findByLogin(user.getLogin()).getName());
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void topUpCashTest() {
        userService.topUpCash(user, new BigDecimal("1000"));
        BigDecimal expected = new BigDecimal("6000");
        BigDecimal actual = user.getCash();
        Assert.assertEquals(expected, actual);
    }
}
