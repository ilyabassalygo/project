package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.DAOHelper;
import by.epam.bassalygo.roulette.dao.SpinDAO;
import by.epam.bassalygo.roulette.dao.UserDAO;
import by.epam.bassalygo.roulette.dao.UserHasBanDAO;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasBan;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class UserHasBanServiceTest {
    private UserHasBanService userHasBanService;
    private static final Logger LOGGER = LogManager.getRootLogger();
    private User user;

    @Before
    public void init() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            userHasBanService = new UserHasBanService();
            UserDAO userDAO = daoHelper.getUserDAO();
            user = userDAO.findByLogin("testguy");
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @After
    public void destroy() {

        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasBanDAO userHasBanDAO = daoHelper.getUserHasBanDAO();
            UserDAO userDAO = daoHelper.getUserDAO();
            userHasBanDAO.delete(user.getUserId());
            user.setCash(new BigDecimal("5000"));
            userDAO.update(user);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void banUserTest() {
        Assert.assertTrue(userHasBanService.banUser(user.getUserId(), "minute mute", String.valueOf(LocalDateTime.now()), ""));
    }

    @Test
    public void unbanUserTest() {
        userHasBanService.banUser(user.getUserId(), "minute mute", String.valueOf(LocalDateTime.now()), "");
        Assert.assertTrue(userHasBanService.unbanUser(user.getUserId()));
    }
}
