package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.*;
import by.epam.bassalygo.roulette.entity.Credit;
import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

public class UserHasCreditServiceTest {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private UserHasCreditService userHasCreditService;
    private User user;

    @Before
    public void init() {
        userHasCreditService = new UserHasCreditService();
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            user = userDAO.findByLogin("testguy");
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @After
    public void destroy() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserHasCreditDAO userHasCreditDAO = daoHelper.getUserHasCreditDAO();
            UserDAO userDAO = daoHelper.getUserDAO();
            userHasCreditDAO.deleteByUser(user.getUserId());
            user.setCash(new BigDecimal("5000"));
            userDAO.update(user);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void takeCreditTest() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            Credit creditType = creditDAO.findCreditByType("short");
            UserHasCredit credit = new UserHasCredit();

            credit.setUserId(user.getUserId());
            credit.setCreditId(creditType.getCreditId());

            LocalDate now = LocalDate.now();
            credit.setTakingDate(Date.valueOf(now));
            now = now.plusDays(creditType.getPaymentDays());
            credit.setRepaymentDate(Date.valueOf(now));
            credit.setCanceled(false);

            Assert.assertTrue(userHasCreditService.takeCredit(user, creditType, credit, new BigDecimal("1000"), new StringBuilder("")));
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void creditPaymentTest() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            CreditDAO creditDAO = daoHelper.getCreditDAO();
            Credit creditType = creditDAO.findCreditByType("middle");
            UserHasCredit credit = new UserHasCredit();

            credit.setUserId(user.getUserId());
            credit.setCreditId(creditType.getCreditId());

            LocalDate now = LocalDate.now();
            credit.setTakingDate(Date.valueOf(now));
            now = now.plusDays(creditType.getPaymentDays());
            credit.setRepaymentDate(Date.valueOf(now));
            credit.setCanceled(false);
            userHasCreditService.takeCredit(user, creditType, credit, new BigDecimal("1000"), new StringBuilder(""));

            String[] payments = {"2"};
            String[] amounts = {"1000"};
            Assert.assertTrue(userHasCreditService.creditPayment(payments, amounts, user, new StringBuilder("")));
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

}
