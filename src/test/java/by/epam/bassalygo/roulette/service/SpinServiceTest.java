package by.epam.bassalygo.roulette.service;

import by.epam.bassalygo.roulette.dao.*;
import by.epam.bassalygo.roulette.entity.*;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SpinServiceTest {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private User user;


    @Before
    public void init() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            UserDAO userDAO = daoHelper.getUserDAO();
            user = userDAO.findByLogin("testguy");
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @After
    public void destroy() {
        try (DAOHelper daoHelper = new DAOHelper()) {
            SpinDAO spinDAO = daoHelper.getSpinDAO();
            UserDAO userDAO = daoHelper.getUserDAO();
            spinDAO.deleteByUser(user.getUserId());
            user.setCash(new BigDecimal("5000"));
            userDAO.update(user);
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void spinTest() {
        SpinService spinService = new SpinService();
        Bet bet = new Bet();
        bet.setBetTypeId(1);
        bet.setUserId(user.getUserId());
        bet.setCells("36");
        bet.setBetSize(new BigDecimal("1000"));
        bet.setWinSize(new BigDecimal("36000"));
        List<Bet> bets = new ArrayList<>();
        bets.add(bet);

        int winCell = 36;

        BigDecimal totalWin = new BigDecimal("0");

        Spin spin = new Spin(Id.generateId(), winCell, totalWin,
                Date.valueOf(LocalDate.now()), user.getUserId());

        Assert.assertTrue("spin method doesn't work correctly", spinService.spin(spin, bets, user, winCell, totalWin));
    }
}
