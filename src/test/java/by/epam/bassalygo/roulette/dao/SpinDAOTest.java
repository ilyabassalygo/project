package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Spin;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;

public class SpinDAOTest {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private Spin spin;
    private SpinDAO spinDAO;


    @Before
    public void init() {
        spinDAO = new SpinDAO();
    }

    @After
    public void destroy() {
        try {
            if (spin != null) {
                spin = spinDAO.findById(999);
                spinDAO.delete(spin.getSpinId());
                spin = null;
            }
            spinDAO.close();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void create() {
        spin = new Spin();
        spin.setSpinId(999);
        spin.setWinCellNumber(12);
        spin.setWinAmount(new BigDecimal("1000"));
        spin.setDate(Date.valueOf("2017-04-20"));
        spin.setUserId(55);

        try {
            Assert.assertTrue(spinDAO.create(spin));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void spinsCountByUserTest() {
        spin = new Spin();
        spin.setSpinId(999);
        spin.setWinCellNumber(12);
        spin.setWinAmount(new BigDecimal("1000"));
        spin.setDate(Date.valueOf("2017-04-20"));
        spin.setUserId(55);
        try {
            spinDAO.create(spin);
            BigDecimal expected = new BigDecimal(1);
            Assert.assertEquals(expected, spinDAO.spinsCountByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }

    }

    @Test
    public void findMaxWinByUserTest() {

        spin = new Spin();
        spin.setSpinId(999);
        spin.setWinCellNumber(12);
        spin.setWinAmount(new BigDecimal("1000"));
        spin.setDate(Date.valueOf("2017-04-20"));
        spin.setUserId(55);
        try {
            spinDAO.create(spin);
            BigDecimal expected = new BigDecimal(1000);
            Assert.assertEquals(expected, spinDAO.findMaxWinByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }
}
