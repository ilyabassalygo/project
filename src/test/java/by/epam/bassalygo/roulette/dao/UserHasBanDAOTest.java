package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.entity.UserHasBan;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;

public class UserHasBanDAOTest {

    private static final int TEST_USER_ID = 55;
    private static final Logger LOGGER = LogManager.getRootLogger();
    private UserHasBan ban;
    private UserHasBanDAO userHasBanDAO;


    @Before
    public void init() {
        userHasBanDAO = new UserHasBanDAO();
    }

    @After
    public void destroy() {
        try {
            if (ban != null) {
                ban = userHasBanDAO.findByIds(55, 3);
                userHasBanDAO.delete(55);
                ban = null;
            }
            userHasBanDAO.close();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void createTest() {
        ban = new UserHasBan();
        ban.setBanId(3);
        ban.setUserId(55);
        ban.setBanDate(Timestamp.valueOf("1900-11-11 00:00:00"));
        ban.setUnbanDate(Timestamp.valueOf("1900-11-11 00:00:00"));
        ban.setReason("reason");

        try {
            Assert.assertTrue(userHasBanDAO.create(ban));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void updateTest() {

        ban = new UserHasBan();
        ban.setBanId(3);
        ban.setUserId(55);
        ban.setBanDate(Timestamp.valueOf("1900-11-11 00:00:00"));
        ban.setUnbanDate(Timestamp.valueOf("1900-11-11 00:00:00"));
        ban.setReason("reason");
        try {
            userHasBanDAO.create(ban);
            ban.setReason("new reason");
            userHasBanDAO.update(ban);
            Assert.assertEquals(ban, userHasBanDAO.findByIds(55, 3));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

}
