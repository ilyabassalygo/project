package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.Bet;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class BetDAOTest {
    private static final Logger LOGGER = LogManager.getRootLogger();
    private Bet bet;
    private BetDAO betDAO;

    @Before
    public void init() {
        betDAO = new BetDAO();
    }

    @After
    public void destroy() {
        try {
            if (bet != null) {
                bet = betDAO.findById(999);
                betDAO.deleteByUser(55);
                bet = null;
            }
            betDAO.close();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void create() {
        bet = new Bet();
        bet.setBetId(999);
        bet.setUserId(55);
        bet.setBetTypeId(1);
        bet.setCells("36");
        bet.setBetSize(new BigDecimal(1000));
        bet.setWinSize(new BigDecimal(1000));
        bet.setSpinId(1);

        try {
            Assert.assertTrue(betDAO.create(bet));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void betsCountByUserTest() {
        bet = new Bet();
        bet.setBetId(999);
        bet.setUserId(55);
        bet.setBetTypeId(1);
        bet.setCells("36");
        bet.setBetSize(new BigDecimal(1000));
        bet.setWinSize(new BigDecimal(3000));
        bet.setSpinId(1);
        try {
            betDAO.create(bet);
            Assert.assertEquals(new BigDecimal(1), betDAO.betsCountByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void findMaxWinByUserTest() {

        bet = new Bet();
        bet.setBetId(999);
        bet.setUserId(55);
        bet.setBetTypeId(1);
        bet.setCells("36");
        bet.setBetSize(new BigDecimal(1000));
        bet.setWinSize(new BigDecimal(3000));
        bet.setSpinId(1);
        try {
            betDAO.create(bet);
            Assert.assertEquals(new BigDecimal(3000), betDAO.findMaxWinByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void winBetsCountByUserTest() {

        bet = new Bet();
        bet.setBetId(999);
        bet.setUserId(55);
        bet.setBetTypeId(1);
        bet.setCells("36");
        bet.setBetSize(new BigDecimal(1000));
        bet.setWinSize(new BigDecimal(3000));
        bet.setSpinId(1);
        try {
            betDAO.create(bet);
            Assert.assertEquals(new BigDecimal(1), betDAO.winBetsCountByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void findMaxBetSizeByUserTest() {

        bet = new Bet();
        bet.setBetId(999);
        bet.setUserId(55);
        bet.setBetTypeId(1);
        bet.setCells("36");
        bet.setBetSize(new BigDecimal(1000));
        bet.setWinSize(new BigDecimal(3000));
        bet.setSpinId(1);
        try {
            betDAO.create(bet);
            Assert.assertEquals(new BigDecimal(1000), betDAO.findMaxBetSizeByUser(55));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }
}
