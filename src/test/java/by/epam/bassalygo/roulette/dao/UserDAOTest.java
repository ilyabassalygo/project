package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.User;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;

public class UserDAOTest {
    private static final int TEST_USER_ID = 55;
    private static final Logger LOGGER = LogManager.getRootLogger();
    private User user;
    private UserDAO userDAO;


    @Before
    public void init() {
        userDAO = new UserDAO();
    }

    @After
    public void destroy() {
        try {
            if (user != null) {
                user = userDAO.findByLogin(user.getLogin());
                userDAO.delete(user.getUserId());
                user = null;
            }
            userDAO.close();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void createTest() {
        try {
            user = new User();
            user.setLogin("test-login");
            user.setPassword("");
            user.setBirthDate(Date.valueOf("1900-12-12"));
            user.setName("name");
            user.setRole("user");
            user.setCash(BigDecimal.ZERO);
            user.setEmail("");
            user.setImgPath("");

            Assert.assertTrue(userDAO.create(user));

        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void findByIdTest() {
        try {

            User user = new User();
            user.setUserId(55);
            user.setLogin("testguy");
            user.setPassword("4ffd32cd3c51cfac95e2166d15b082ae");
            user.setBirthDate(Date.valueOf("1995-11-11"));
            user.setName("test");
            user.setRole("user");
            user.setCash(new BigDecimal("5000"));
            user.setEmail("test@test.test");
            user.setImgPath("E:\\opt\\Tomcat8\\webapps\\images\\roulette\\jpg.png");

            User userActual = userDAO.findById(TEST_USER_ID);
            Assert.assertEquals(user, userActual);
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void findByLoginTest() {
        try {
            User user = new User();
            user.setUserId(55);
            user.setLogin("testguy");
            user.setPassword("4ffd32cd3c51cfac95e2166d15b082ae");
            user.setBirthDate(Date.valueOf("1995-11-11"));
            user.setName("test");
            user.setRole("user");
            user.setCash(new BigDecimal("5000"));
            user.setEmail("test@test.test");
            user.setImgPath("E:\\opt\\Tomcat8\\webapps\\images\\roulette\\jpg.png");

            User userActual = userDAO.findByLogin("testguy");
            Assert.assertEquals(user, userActual);
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void updateTest() {
        try {
            user = new User();
            user.setLogin("test-login");
            user.setPassword("");
            user.setBirthDate(Date.valueOf("1900-12-12"));
            user.setName("name");
            user.setRole("user");
            user.setCash(BigDecimal.ZERO);
            user.setEmail("");
            user.setImgPath("");

            userDAO.create(user);

            User updated = userDAO.findByLogin("test-login");
            updated.setName("newName");
            userDAO.update(updated);

            user = userDAO.findByLogin("test-login");

            Assert.assertEquals(updated, user);
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }
}
