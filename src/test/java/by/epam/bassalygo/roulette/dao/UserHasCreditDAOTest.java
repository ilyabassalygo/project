package by.epam.bassalygo.roulette.dao;

import by.epam.bassalygo.roulette.entity.UserHasCredit;
import by.epam.bassalygo.roulette.exception.RouletteDAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;

public class UserHasCreditDAOTest {

    private static final int TEST_USER_ID = 55;
    private static final Logger LOGGER = LogManager.getRootLogger();
    private UserHasCredit userHasCredit;
    private UserHasCreditDAO userHasCreditDAO;


    @Before
    public void init() {
        userHasCreditDAO = new UserHasCreditDAO();
    }

    @After
    public void destroy() {
        try {
            if (userHasCreditDAO != null) {
                userHasCreditDAO.deleteByUser(55);
                userHasCredit = null;
            }
            userHasCreditDAO.close();
        } catch (RouletteDAOException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void createTest() {
        userHasCredit = new UserHasCredit();
        userHasCredit.setUserId(55);
        userHasCredit.setCreditId(3);
        userHasCredit.setTakingDate(Date.valueOf("2017-05-08"));
        userHasCredit.setRepaymentDate(Date.valueOf("2017-11-05"));
        userHasCredit.setPaymentAmount(new BigDecimal("1000.00"));
        userHasCredit.setPayment(BigDecimal.ZERO);
        userHasCredit.setCanceled(false);

        try {
            Assert.assertTrue(userHasCreditDAO.create(userHasCredit));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }

    @Test
    public void updateTest() {

        userHasCredit = new UserHasCredit();
        userHasCredit.setUserId(55);
        userHasCredit.setCreditId(3);
        userHasCredit.setTakingDate(Date.valueOf("2017-11-05"));
        userHasCredit.setRepaymentDate(Date.valueOf("2017-11-05"));
        userHasCredit.setPaymentAmount(new BigDecimal("1000.00"));
        userHasCredit.setPayment(BigDecimal.ZERO);
        userHasCredit.setCanceled(false);
        try {
            userHasCreditDAO.create(userHasCredit);
            userHasCredit.setPayment(new BigDecimal("1000.00"));
            userHasCreditDAO.update(userHasCredit);
            Assert.assertEquals(userHasCredit, userHasCreditDAO.findByIds(55, 3));
        } catch (RouletteDAOException e) {
            Assert.fail();
        }
    }
}
